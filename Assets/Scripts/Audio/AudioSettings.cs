using System;
using UnityEngine;

namespace Audio
{
    public static class AudioSettings
    {
        public static bool SoundIsOn
        {
            get => PlayerPrefs.GetInt(SoundKey, 1) != 0;
            set
            {
                if (SoundIsOn == value) return;
                PlayerPrefs.SetInt(SoundKey, value ? 1 : 0);
                PlayerPrefs.Save();
                SoundChanged?.Invoke();
            }
        }

        public static bool MusicIsOn
        {
            get => PlayerPrefs.GetInt(MusicKey, 1) != 0;
            set
            {
                if (MusicIsOn == value) return;
                PlayerPrefs.SetInt(MusicKey, value ? 1 : 0);
                PlayerPrefs.Save();
                MusicChanged?.Invoke();
            }
        }

        public static event Action SoundChanged;
        public static event Action MusicChanged;

        private const string SoundKey = "SoundOn";
        private const string MusicKey = "MusicOn";
    }
}