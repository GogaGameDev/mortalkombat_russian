using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInTime : MonoBehaviour
{
   [SerializeField] float time=-1;
    void Start()
    {
        if (time != -1)
        {
            Destroy(this.gameObject, time);
        }
    }

    public void DestroySelf()
    {
        Destroy(this.gameObject);

    }
    
}
