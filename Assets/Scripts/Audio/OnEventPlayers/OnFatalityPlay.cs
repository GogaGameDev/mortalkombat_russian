﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace Audio
{
    public class OnFatalityPlay : OnEventPlay
    {
        [SerializeField] private SpecialBase _specialBase;
        [ShowIf(nameof(_playOnStop))] [SerializeField] private Fighter _fighter;
        [SerializeField] private bool _playOnStop;
        protected override void SubscribeToEvent()
        {
            if (_playOnStop)
                _specialBase.OnFatalityStarted.AddListener(OnMoveStopPlay);
            else
                _specialBase.OnFatalityStarted.AddListener(PlaySound);

        }

        private void OnMoveStopPlay()
        {
            _fighter.OnMoveStop.AddListener(PlayAndUnsubscribe);
        }

        private void PlayAndUnsubscribe()
        {
            PlaySound();
            _fighter.OnMoveStop.RemoveListener(PlayAndUnsubscribe);
        }
    }
}