// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Inputs/FighterControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @FighterControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @FighterControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""FighterControls"",
    ""maps"": [
        {
            ""name"": ""Fighter"",
            ""id"": ""13bbc7aa-74f9-49b6-85db-128e8227d311"",
            ""actions"": [
                {
                    ""name"": ""HandAttack"",
                    ""type"": ""Button"",
                    ""id"": ""33f3c055-4246-4a08-95b7-49d72cbf6aae"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""HandAttack_2"",
                    ""type"": ""Button"",
                    ""id"": ""a2b45ec8-dc68-4265-adb4-9dc51f143d1c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LegAttack"",
                    ""type"": ""Button"",
                    ""id"": ""3bd54eeb-d4a1-4ac0-81cb-16abb93914fc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LegAttack_2"",
                    ""type"": ""Button"",
                    ""id"": ""192cf5c4-0aaa-426f-ad2d-10525285ea13"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move_Left Start"",
                    ""type"": ""Button"",
                    ""id"": ""b124153e-d9ea-4583-b92a-0002e95fba52"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                },
                {
                    ""name"": ""Move_Left End"",
                    ""type"": ""Button"",
                    ""id"": ""d2b6debf-c19b-42a4-bdd2-a963061acac8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                },
                {
                    ""name"": ""Move_Right_Start"",
                    ""type"": ""Button"",
                    ""id"": ""2caf684f-a8b7-458c-bdaf-b375fb26d99a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move_Right_End"",
                    ""type"": ""Button"",
                    ""id"": ""262dc643-c1d1-4745-bae9-0712c953bca9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""8ab47191-9d34-4cf4-bd78-f86639f4ec9e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move_Crouch_Start"",
                    ""type"": ""Button"",
                    ""id"": ""5e441bed-c087-47c4-b8f2-10d06aa413a5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move_Crouch_End"",
                    ""type"": ""Button"",
                    ""id"": ""00ac25b2-1b69-4b6e-a521-8f31d99e918d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Block_1"",
                    ""type"": ""Button"",
                    ""id"": ""b72d22b9-2b45-46ea-9197-5f6de3c3ede9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UnBlock_1"",
                    ""type"": ""Button"",
                    ""id"": ""27ff75c1-6192-4677-8301-e09b58d8e0c3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Block_2"",
                    ""type"": ""Button"",
                    ""id"": ""f16d7ce3-c1d5-4066-8e52-49d5a0aa412d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UnBlock_2"",
                    ""type"": ""Button"",
                    ""id"": ""9036d950-613e-4b64-80ab-2224c0a203e4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Block_3"",
                    ""type"": ""Button"",
                    ""id"": ""d38b26c2-b0f0-4323-8841-a1d0f57bf876"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UnBlock_3"",
                    ""type"": ""Button"",
                    ""id"": ""bd9ddd1c-556b-4e15-94d1-142edac6fc14"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Block_4"",
                    ""type"": ""Button"",
                    ""id"": ""0c1ba171-ce0b-4bfe-91a8-40446a362a0f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UnBlock_4"",
                    ""type"": ""Button"",
                    ""id"": ""3b9369d0-dbcd-4896-9d72-d62baec02de4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""52bab5c1-5a20-46a3-80ec-3a9c94557880"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HandAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2f3a8822-ed8d-4803-b461-7c5ca9ee8a1d"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HandAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""263d531f-756e-4cce-8d91-7adc42a7aefd"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LegAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a36a2b00-722e-4428-80f0-7f5e5bed801f"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LegAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b714ffca-d813-4e19-8a87-d003123632b3"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Left Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3651fb2-43d2-470d-96de-a42dce23d7ee"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Left Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b7fb53ab-a566-4255-9328-7e522fc1d5d6"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Left Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2b11d60e-ddfd-4f2c-9244-cc8e6c2d97d2"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Right_Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""877ca242-9086-494e-b1f6-1a8a537b085e"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Right_Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f37230d5-730c-447f-b236-1dc0ea043c68"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Right_Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8d56268a-4218-483b-a1d3-eda9a7b8a514"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef60e947-6821-4afb-872c-f603a3693a5b"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ba74f002-3397-4fab-9186-c47d7171a816"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4f22de91-8a66-49db-bce6-22bbe7f0146b"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Crouch_Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7ec80467-241c-4c0c-a2f5-925e6eaccd67"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Crouch_Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""26f0131e-ced0-4899-aabf-ed504fb5cedf"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Crouch_Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b5417acf-4077-4253-91d7-606473748786"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Left End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""89db094a-7754-425c-9869-49b419feee84"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Left End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b4c7eaf7-3578-4260-a47e-ed375e5a266d"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Left End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cd0bf6fe-3833-42ed-a694-2b6b3c39ac63"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Right_End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1956c1ee-e6c0-44da-8bad-8b91794ae9ad"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Right_End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""40573ce6-749f-4508-a3e0-450e7e9f90b8"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Right_End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""61c5ad0f-a605-4840-9b7d-3869e1af2230"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Crouch_End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5f6cc4d4-2871-4cfc-b1b6-9d2fb55097df"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Crouch_End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c10aa111-3e75-4525-ad41-a3e8c302fec1"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Crouch_End"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a8dc85ca-7296-482d-98c1-a74543f535da"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""913e2939-2b94-435e-a393-efffe96ca4e8"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2df840a4-7f55-4a7b-acd4-6dabeccd124d"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HandAttack_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""63f359b4-b6d0-43a6-81df-52d9c0265ecb"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HandAttack_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a480a658-fa4c-4bcd-bbe2-a9fe8e5c55af"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f3b58ad8-a38b-414e-864d-f13519828409"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""af1fa336-8e58-4e04-845c-41bd7e465794"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e02bb0a-8764-4278-8a58-f0ea134634a3"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b9094e61-c8cd-4882-ac62-4fd5fcba4f4b"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b63e8fb9-aed3-4432-acb7-17a024573035"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""337f7d06-ac93-441c-a3be-6191bab7082d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""38ab13a9-2c59-4822-b936-9275f902a0ff"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ff7fda5f-0cff-49bc-86da-7e9edb7bebcc"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""66397773-f346-4a33-aa1f-0fd3794dda37"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9c9f6925-d35a-449a-8681-75e6577e9f31"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b0ccd1a3-287f-4030-9274-1af020d153bb"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1f62d8d2-dd72-4b6f-88a6-ebed445a9ff1"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c4d9b778-abbb-4df8-9dd6-18811a3cba58"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UnBlock_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a2a0971d-bf3e-439e-83bb-2f39e5c1e7cb"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LegAttack_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""27181108-e7c0-4c2d-951e-8d548a8fbebc"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LegAttack_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""CharacterSelection"",
            ""bindingGroup"": ""CharacterSelection"",
            ""devices"": []
        },
        {
            ""name"": ""FighterControls"",
            ""bindingGroup"": ""FighterControls"",
            ""devices"": []
        }
    ]
}");
        // Fighter
        m_Fighter = asset.FindActionMap("Fighter", throwIfNotFound: true);
        m_Fighter_HandAttack = m_Fighter.FindAction("HandAttack", throwIfNotFound: true);
        m_Fighter_HandAttack_2 = m_Fighter.FindAction("HandAttack_2", throwIfNotFound: true);
        m_Fighter_LegAttack = m_Fighter.FindAction("LegAttack", throwIfNotFound: true);
        m_Fighter_LegAttack_2 = m_Fighter.FindAction("LegAttack_2", throwIfNotFound: true);
        m_Fighter_Move_LeftStart = m_Fighter.FindAction("Move_Left Start", throwIfNotFound: true);
        m_Fighter_Move_LeftEnd = m_Fighter.FindAction("Move_Left End", throwIfNotFound: true);
        m_Fighter_Move_Right_Start = m_Fighter.FindAction("Move_Right_Start", throwIfNotFound: true);
        m_Fighter_Move_Right_End = m_Fighter.FindAction("Move_Right_End", throwIfNotFound: true);
        m_Fighter_Jump = m_Fighter.FindAction("Jump", throwIfNotFound: true);
        m_Fighter_Move_Crouch_Start = m_Fighter.FindAction("Move_Crouch_Start", throwIfNotFound: true);
        m_Fighter_Move_Crouch_End = m_Fighter.FindAction("Move_Crouch_End", throwIfNotFound: true);
        m_Fighter_Block_1 = m_Fighter.FindAction("Block_1", throwIfNotFound: true);
        m_Fighter_UnBlock_1 = m_Fighter.FindAction("UnBlock_1", throwIfNotFound: true);
        m_Fighter_Block_2 = m_Fighter.FindAction("Block_2", throwIfNotFound: true);
        m_Fighter_UnBlock_2 = m_Fighter.FindAction("UnBlock_2", throwIfNotFound: true);
        m_Fighter_Block_3 = m_Fighter.FindAction("Block_3", throwIfNotFound: true);
        m_Fighter_UnBlock_3 = m_Fighter.FindAction("UnBlock_3", throwIfNotFound: true);
        m_Fighter_Block_4 = m_Fighter.FindAction("Block_4", throwIfNotFound: true);
        m_Fighter_UnBlock_4 = m_Fighter.FindAction("UnBlock_4", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Fighter
    private readonly InputActionMap m_Fighter;
    private IFighterActions m_FighterActionsCallbackInterface;
    private readonly InputAction m_Fighter_HandAttack;
    private readonly InputAction m_Fighter_HandAttack_2;
    private readonly InputAction m_Fighter_LegAttack;
    private readonly InputAction m_Fighter_LegAttack_2;
    private readonly InputAction m_Fighter_Move_LeftStart;
    private readonly InputAction m_Fighter_Move_LeftEnd;
    private readonly InputAction m_Fighter_Move_Right_Start;
    private readonly InputAction m_Fighter_Move_Right_End;
    private readonly InputAction m_Fighter_Jump;
    private readonly InputAction m_Fighter_Move_Crouch_Start;
    private readonly InputAction m_Fighter_Move_Crouch_End;
    private readonly InputAction m_Fighter_Block_1;
    private readonly InputAction m_Fighter_UnBlock_1;
    private readonly InputAction m_Fighter_Block_2;
    private readonly InputAction m_Fighter_UnBlock_2;
    private readonly InputAction m_Fighter_Block_3;
    private readonly InputAction m_Fighter_UnBlock_3;
    private readonly InputAction m_Fighter_Block_4;
    private readonly InputAction m_Fighter_UnBlock_4;
    public struct FighterActions
    {
        private @FighterControls m_Wrapper;
        public FighterActions(@FighterControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @HandAttack => m_Wrapper.m_Fighter_HandAttack;
        public InputAction @HandAttack_2 => m_Wrapper.m_Fighter_HandAttack_2;
        public InputAction @LegAttack => m_Wrapper.m_Fighter_LegAttack;
        public InputAction @LegAttack_2 => m_Wrapper.m_Fighter_LegAttack_2;
        public InputAction @Move_LeftStart => m_Wrapper.m_Fighter_Move_LeftStart;
        public InputAction @Move_LeftEnd => m_Wrapper.m_Fighter_Move_LeftEnd;
        public InputAction @Move_Right_Start => m_Wrapper.m_Fighter_Move_Right_Start;
        public InputAction @Move_Right_End => m_Wrapper.m_Fighter_Move_Right_End;
        public InputAction @Jump => m_Wrapper.m_Fighter_Jump;
        public InputAction @Move_Crouch_Start => m_Wrapper.m_Fighter_Move_Crouch_Start;
        public InputAction @Move_Crouch_End => m_Wrapper.m_Fighter_Move_Crouch_End;
        public InputAction @Block_1 => m_Wrapper.m_Fighter_Block_1;
        public InputAction @UnBlock_1 => m_Wrapper.m_Fighter_UnBlock_1;
        public InputAction @Block_2 => m_Wrapper.m_Fighter_Block_2;
        public InputAction @UnBlock_2 => m_Wrapper.m_Fighter_UnBlock_2;
        public InputAction @Block_3 => m_Wrapper.m_Fighter_Block_3;
        public InputAction @UnBlock_3 => m_Wrapper.m_Fighter_UnBlock_3;
        public InputAction @Block_4 => m_Wrapper.m_Fighter_Block_4;
        public InputAction @UnBlock_4 => m_Wrapper.m_Fighter_UnBlock_4;
        public InputActionMap Get() { return m_Wrapper.m_Fighter; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(FighterActions set) { return set.Get(); }
        public void SetCallbacks(IFighterActions instance)
        {
            if (m_Wrapper.m_FighterActionsCallbackInterface != null)
            {
                @HandAttack.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnHandAttack;
                @HandAttack.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnHandAttack;
                @HandAttack.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnHandAttack;
                @HandAttack_2.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnHandAttack_2;
                @HandAttack_2.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnHandAttack_2;
                @HandAttack_2.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnHandAttack_2;
                @LegAttack.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnLegAttack;
                @LegAttack.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnLegAttack;
                @LegAttack.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnLegAttack;
                @LegAttack_2.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnLegAttack_2;
                @LegAttack_2.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnLegAttack_2;
                @LegAttack_2.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnLegAttack_2;
                @Move_LeftStart.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_LeftStart;
                @Move_LeftStart.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_LeftStart;
                @Move_LeftStart.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_LeftStart;
                @Move_LeftEnd.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_LeftEnd;
                @Move_LeftEnd.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_LeftEnd;
                @Move_LeftEnd.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_LeftEnd;
                @Move_Right_Start.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Right_Start;
                @Move_Right_Start.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Right_Start;
                @Move_Right_Start.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Right_Start;
                @Move_Right_End.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Right_End;
                @Move_Right_End.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Right_End;
                @Move_Right_End.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Right_End;
                @Jump.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnJump;
                @Move_Crouch_Start.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Crouch_Start;
                @Move_Crouch_Start.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Crouch_Start;
                @Move_Crouch_Start.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Crouch_Start;
                @Move_Crouch_End.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Crouch_End;
                @Move_Crouch_End.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Crouch_End;
                @Move_Crouch_End.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnMove_Crouch_End;
                @Block_1.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_1;
                @Block_1.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_1;
                @Block_1.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_1;
                @UnBlock_1.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_1;
                @UnBlock_1.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_1;
                @UnBlock_1.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_1;
                @Block_2.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_2;
                @Block_2.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_2;
                @Block_2.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_2;
                @UnBlock_2.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_2;
                @UnBlock_2.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_2;
                @UnBlock_2.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_2;
                @Block_3.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_3;
                @Block_3.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_3;
                @Block_3.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_3;
                @UnBlock_3.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_3;
                @UnBlock_3.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_3;
                @UnBlock_3.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_3;
                @Block_4.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_4;
                @Block_4.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_4;
                @Block_4.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnBlock_4;
                @UnBlock_4.started -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_4;
                @UnBlock_4.performed -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_4;
                @UnBlock_4.canceled -= m_Wrapper.m_FighterActionsCallbackInterface.OnUnBlock_4;
            }
            m_Wrapper.m_FighterActionsCallbackInterface = instance;
            if (instance != null)
            {
                @HandAttack.started += instance.OnHandAttack;
                @HandAttack.performed += instance.OnHandAttack;
                @HandAttack.canceled += instance.OnHandAttack;
                @HandAttack_2.started += instance.OnHandAttack_2;
                @HandAttack_2.performed += instance.OnHandAttack_2;
                @HandAttack_2.canceled += instance.OnHandAttack_2;
                @LegAttack.started += instance.OnLegAttack;
                @LegAttack.performed += instance.OnLegAttack;
                @LegAttack.canceled += instance.OnLegAttack;
                @LegAttack_2.started += instance.OnLegAttack_2;
                @LegAttack_2.performed += instance.OnLegAttack_2;
                @LegAttack_2.canceled += instance.OnLegAttack_2;
                @Move_LeftStart.started += instance.OnMove_LeftStart;
                @Move_LeftStart.performed += instance.OnMove_LeftStart;
                @Move_LeftStart.canceled += instance.OnMove_LeftStart;
                @Move_LeftEnd.started += instance.OnMove_LeftEnd;
                @Move_LeftEnd.performed += instance.OnMove_LeftEnd;
                @Move_LeftEnd.canceled += instance.OnMove_LeftEnd;
                @Move_Right_Start.started += instance.OnMove_Right_Start;
                @Move_Right_Start.performed += instance.OnMove_Right_Start;
                @Move_Right_Start.canceled += instance.OnMove_Right_Start;
                @Move_Right_End.started += instance.OnMove_Right_End;
                @Move_Right_End.performed += instance.OnMove_Right_End;
                @Move_Right_End.canceled += instance.OnMove_Right_End;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Move_Crouch_Start.started += instance.OnMove_Crouch_Start;
                @Move_Crouch_Start.performed += instance.OnMove_Crouch_Start;
                @Move_Crouch_Start.canceled += instance.OnMove_Crouch_Start;
                @Move_Crouch_End.started += instance.OnMove_Crouch_End;
                @Move_Crouch_End.performed += instance.OnMove_Crouch_End;
                @Move_Crouch_End.canceled += instance.OnMove_Crouch_End;
                @Block_1.started += instance.OnBlock_1;
                @Block_1.performed += instance.OnBlock_1;
                @Block_1.canceled += instance.OnBlock_1;
                @UnBlock_1.started += instance.OnUnBlock_1;
                @UnBlock_1.performed += instance.OnUnBlock_1;
                @UnBlock_1.canceled += instance.OnUnBlock_1;
                @Block_2.started += instance.OnBlock_2;
                @Block_2.performed += instance.OnBlock_2;
                @Block_2.canceled += instance.OnBlock_2;
                @UnBlock_2.started += instance.OnUnBlock_2;
                @UnBlock_2.performed += instance.OnUnBlock_2;
                @UnBlock_2.canceled += instance.OnUnBlock_2;
                @Block_3.started += instance.OnBlock_3;
                @Block_3.performed += instance.OnBlock_3;
                @Block_3.canceled += instance.OnBlock_3;
                @UnBlock_3.started += instance.OnUnBlock_3;
                @UnBlock_3.performed += instance.OnUnBlock_3;
                @UnBlock_3.canceled += instance.OnUnBlock_3;
                @Block_4.started += instance.OnBlock_4;
                @Block_4.performed += instance.OnBlock_4;
                @Block_4.canceled += instance.OnBlock_4;
                @UnBlock_4.started += instance.OnUnBlock_4;
                @UnBlock_4.performed += instance.OnUnBlock_4;
                @UnBlock_4.canceled += instance.OnUnBlock_4;
            }
        }
    }
    public FighterActions @Fighter => new FighterActions(this);
    private int m_CharacterSelectionSchemeIndex = -1;
    public InputControlScheme CharacterSelectionScheme
    {
        get
        {
            if (m_CharacterSelectionSchemeIndex == -1) m_CharacterSelectionSchemeIndex = asset.FindControlSchemeIndex("CharacterSelection");
            return asset.controlSchemes[m_CharacterSelectionSchemeIndex];
        }
    }
    private int m_FighterControlsSchemeIndex = -1;
    public InputControlScheme FighterControlsScheme
    {
        get
        {
            if (m_FighterControlsSchemeIndex == -1) m_FighterControlsSchemeIndex = asset.FindControlSchemeIndex("FighterControls");
            return asset.controlSchemes[m_FighterControlsSchemeIndex];
        }
    }
    public interface IFighterActions
    {
        void OnHandAttack(InputAction.CallbackContext context);
        void OnHandAttack_2(InputAction.CallbackContext context);
        void OnLegAttack(InputAction.CallbackContext context);
        void OnLegAttack_2(InputAction.CallbackContext context);
        void OnMove_LeftStart(InputAction.CallbackContext context);
        void OnMove_LeftEnd(InputAction.CallbackContext context);
        void OnMove_Right_Start(InputAction.CallbackContext context);
        void OnMove_Right_End(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnMove_Crouch_Start(InputAction.CallbackContext context);
        void OnMove_Crouch_End(InputAction.CallbackContext context);
        void OnBlock_1(InputAction.CallbackContext context);
        void OnUnBlock_1(InputAction.CallbackContext context);
        void OnBlock_2(InputAction.CallbackContext context);
        void OnUnBlock_2(InputAction.CallbackContext context);
        void OnBlock_3(InputAction.CallbackContext context);
        void OnUnBlock_3(InputAction.CallbackContext context);
        void OnBlock_4(InputAction.CallbackContext context);
        void OnUnBlock_4(InputAction.CallbackContext context);
    }
}