﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Audio.SoundPlayers
{
    /// <summary>
    /// When enabled, random sound from the array is played on awake
    /// </summary>
    public class OnStartRandomSoundPlayer : SoundPlayer
    {
        [SerializeField] private AudioClip[] _audioClips;

        private void Start()
        {
            Play();
        }

        public override void Play()
        {
            Play(_audioClips[Random.Range(0, _audioClips.Length)]);
        }
    }
}