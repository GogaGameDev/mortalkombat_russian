using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterAnimation : MonoBehaviour
{
    [SerializeField] Animator _animator;
    private bool _isMovementState = false;
    private bool _isAttackState = false;
    private Fighter _fighter = null;

    private void Update()
    {
#if UNITY_EDITOR
        if (_fighter._playerID == 1)
        {
          //  Debug.Log(_animator.GetCurrentAnimatorClipInfo(0)[0].clip.name);
        }
#endif
    }
    void Start()
    {

        _fighter = this.GetComponent<Fighter>();


        _fighter.OnJump.AddListener(() =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }
            // Debug.Log("OnJump");
            _animator.SetTrigger("jump_trigger");
            _animator.SetBool("jump_bool", true);
            AttackAnimationStop();
            if (_isMovementState)
            {
                return;
            }
            _isMovementState = true;
            _isAttackState = false;

        });
        _fighter.OnSomersault.AddListener((isMoving, isMovingBackWard, isGrounded) =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }

            if (isMovingBackWard)
            {
                _animator.SetBool("somersault_bool", false);
                _animator.SetBool("somersault_backward_bool", isMoving);
            }
            else
            {
                _animator.SetBool("somersault_backward_bool", false);
                _animator.SetBool("somersault_bool", isMoving);
            }



        });
        _fighter.OnLanding.AddListener((isMoving) =>
        {
            if (_fighter.IsStunned || _fighter.IsKnockedDown || _fighter.IsUnderSpecial)
            {
                return;
            }
            // Debug.Log("landing_animation");
            AttackAnimationStop();
            _animator.SetBool("somersault_bool", false);
            _animator.SetBool("jump_bool", false);
            _animator.SetTrigger(isMoving ? "IDL" : "landing_trigger");

        });

        _fighter.OnMoveStart.AddListener((isMovingBackWard) =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }

            _animator.SetBool(isMovingBackWard ? "walking_backward_bool" : "walking_bool", true);
            _animator.SetBool(isMovingBackWard ? "walking_bool" : "walking_backward_bool", false);
            if (_isMovementState)
            {
                return;
            }
            _isMovementState = true;
            _isAttackState = false;
            _animator.SetTrigger("movement_state_trigger");

        });
        _fighter.OnMoveStop.AddListener(() =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }
            _animator.SetBool("walking_bool", false);
            _animator.SetBool("walking_backward_bool", false);
            _isMovementState = false;
        });

        _fighter.OnGetup.AddListener(() =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }
            _animator.SetTrigger("getup_trigger");
        });
        _fighter.OnCrouch.AddListener((isCroucning) =>
        {
            if (!isCroucning)
            {
                _animator.ResetTrigger("movement_state_trigger");
                _animator.SetBool("crouch_bool", isCroucning);

            }

            if (_fighter.IsUnderSpecial)
            {
                return;
            }

            if (_fighter.IsDoingSpecial)
            {
                return;
            }
            Debug.Log("activateCrouch Anim");


            if (!isCroucning)
            {
                return;
            }
            else
            {
                Debug.Log("Crouch Anim");
                _animator.SetTrigger("movement_state_trigger");
                _animator.SetBool("crouch_bool", isCroucning);

            }
            if (_isMovementState)
            {
                return;
            }

            _isMovementState = true;
            _isAttackState = false;

        });

        _fighter.OnAttack.AddListener((attackType) =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }
            _isMovementState = false;
            // Debug.Log(attackType);
            if (_fighter.IsDoingSpecial)
            {
                return;
            }

            _animator.SetBool("attack_bool", false);
            _animator.SetBool("hand_attack_bool", false);
            _animator.SetBool("hand_attack_low_bool", false);
            _animator.SetBool("hand_attack_uppercut_bool", false);
            _animator.SetBool("hand_attack_air_bool", false);
            _animator.SetBool("leg_attack_air_bool", false);
            _animator.SetBool("leg_attack_bool", false);
            _animator.SetBool("leg_attack_low_bool", false);


            switch (attackType)
            {
                case AttackType.Hand_Low:
                    _animator.SetBool("hand_attack_bool", true);
                    _animator.SetBool("hand_attack_low_bool", true);
                    _animator.SetTrigger("attack_state_trigger");
                    break;
                case AttackType.Hand_Low_UpperCut:
                    //   _animator.SetBool("hand_attack_bool", true);
                    _animator.SetBool("hand_attack_uppercut_bool", true);
                    //    _animator.SetBool("attack_bool", true);
                    // _animator.SetTrigger("attack_state_trigger");
                    break;
                case AttackType.Hand_Mid:
                    _animator.SetBool("hand_attack_bool", true);
                    _animator.SetBool("hand_attack_mid_bool", true);
                    _animator.SetBool("attack_bool", true);
                    _animator.SetTrigger("attack_state_trigger");
                    break;
                case AttackType.Hand_Air:
                    _animator.SetBool("hand_attack_bool", true);
                    _animator.SetBool("hand_attack_air_bool", true);
                    _animator.SetBool("attack_bool", true);
                    _animator.SetTrigger("attack_state_trigger");
                    break;
                case AttackType.Leg_Low:
                    _animator.SetBool("leg_attack_bool", true);
                    _animator.SetBool("leg_attack_low_bool", true);
                    _animator.SetBool("attack_bool", true);
                    _animator.SetTrigger("attack_state_trigger");
                    break;
                case AttackType.Leg_Mid:
                    _animator.SetBool("leg_attack_bool", true);
                    _animator.SetBool("leg_attack_mid_bool", true);
                    _animator.SetBool("attack_bool", true);
                    _animator.SetTrigger("attack_state_trigger");
                    break;
                case AttackType.Leg_Air:
                    _animator.SetBool("leg_attack_bool", true);
                    _animator.SetBool("leg_attack_air_bool", true);
                    _animator.SetBool("attack_bool", true);
                    _animator.SetTrigger("attack_state_trigger");
                    break;
            }

            if (_isAttackState)
            {
                return;

            }
            _isAttackState = true;

        });
        _fighter.OnHit.AddListener((hitType) =>
        {
            AttackAnimationStop();

            if (_fighter.IsUnderSpecial)
            {
                return;
            }



            if (hitType == HitType.UpperCut)
            {
                _animator.SetTrigger("reaction_uppercut_trigger");
                return;
            }
            if (_fighter.IsBlocking)
            {
                _animator.SetTrigger("hit_trigger");
                return;
            }

            SetBool("individual_special_bool", false);
            _animator.SetTrigger("reaction_state_trigger");
            switch (hitType)
            {
                case HitType.Low:
                    _animator.SetTrigger("reaction_low_trigger");
                    break;
                case HitType.Mid:
                    _animator.SetTrigger("reaction_mid_trigger");
                    break;
                case HitType.UpperCut:
                    _animator.SetTrigger("reaction_uppercut_trigger");
                    break;
            }

        });

        _fighter.OnDeath.AddListener((playerID) =>
        {
            if (_fighter.IsUnderSpecial || !GameManager.IsEndGame())
            {
                return;
            }

            Debug.Log("STUNNN");
            AttackAnimationStop();
            _animator.SetBool("stunn_bool", true);
            _animator.SetTrigger("stunn_trigger");
        });
        _fighter.OnFinalDeath.AddListener(() =>
        {
            if (_fighter.IsUnderSpecial || _fighter.IsKnockedDown)
            {
                return;
            }

            SetState("FinalDeath");
        });
        _fighter.OnStunn.AddListener(() =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }
            _animator.SetTrigger("stunn_trigger");
        });
        _fighter.OnBlock.AddListener((isBlocking) =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }


            _animator.SetBool("block_bool", isBlocking);

            if (_isMovementState)
            {
                return;
            }
            _isMovementState = true;
            _isAttackState = false;
            _animator.SetTrigger("movement_state_trigger");
        });

        _fighter.Combat.Special.OnFatalityStarted.AddListener(ResetAnimations);

        FighterInputs inputs = this.GetComponent<FighterInputs>();

        inputs.OnAttackEnded.AddListener(() =>
        {
            if (_fighter.IsUnderSpecial)
            {
                return;
            }
            AttackAnimationStop();
        });

        inputs.OnSpecalInput.AddListener((specialType) =>
        {
            if (!_fighter.IsOnGround || _fighter.IsDoingSpecial || _fighter._isAttacking || _fighter.IsUnderSpecial)
            {
                return;
            }
            _isMovementState = false;
            _isAttackState = true;
            _animator.SetTrigger("special_" + specialType + "_trigger");
        });


    }

    public void SetTrigger(string trigger)
    {
        _animator.SetTrigger(trigger);
    }
    public void SetBool(string boolName, bool value)
    {
        _animator.SetBool(boolName, value);
    }
    public void ResetTrigger(string trigger)
    {
        _animator.ResetTrigger(trigger);
    }

    public void ResetAnimations()
    {

        foreach (var parameter in _animator.parameters)
        {
            switch (parameter.type)
            {
                case AnimatorControllerParameterType.Trigger:
                    _animator.ResetTrigger(parameter.name);
                    break;
                case AnimatorControllerParameterType.Bool:
                    _animator.SetBool(parameter.name, false);
                    break;
            }
        }

    }
    public void AttackAnimationStop()
    {

        _animator.SetBool("attack_bool", false);

        _animator.SetBool("hand_attack_air_bool", false);
        _animator.SetBool("hand_attack_mid_bool", false);
        _animator.SetBool("hand_attack_bool", false);
        _animator.SetBool("hand_attack_low_bool", false);
        _animator.SetBool("leg_attack_bool", false);
        _animator.SetBool("leg_attack_mid_bool", false);
        _animator.SetBool("leg_attack_air_bool", false);
        _animator.SetBool("leg_attack_low_bool", false);
        _animator.SetBool("hand_attack_uppercut_bool", false);

        _isAttackState = false;
    }
    public void StopAnimationAttack(string attackType)
    {

        _animator.SetBool("attack_bool", false);
        _animator.SetBool("hand_attack_bool", false);
        _animator.SetBool("leg_attack_bool", false);
        Debug.Log(gameObject.name + " AttackAnimationStop");
        _fighter._isAttacking = false;
        _animator.SetBool(attackType, false);
    }
    public void SetState(string stateName)
    {
        _animator.Play(stateName);
    }
}