﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Audio.SoundPlayers
{
    /// <summary>
    /// Provides a method for sound sequence playing at the start of the round:
    /// "Round [roundID]", "Fight"
    /// </summary>
    public class RoundStartSoundPlayer : SoundPlayer
    {
        [SerializeField] private AudioClip[] _roundPhrases;
        [SerializeField] private AudioClip _fightPhrase;
        [SerializeField] private float _delayAfterRoundNumber = 1.5f;

        private void Start()
        {
            Play();
        }

        /// <summary>
        /// Run it at the start of each round
        /// </summary>
        public override void Play()
        {
            DOTween.Sequence().AppendCallback(() => PlayOnce(_roundPhrases[GameManager.roundID]))
                .AppendInterval(_delayAfterRoundNumber).AppendCallback(() => PlayOnce(_fightPhrase));
        }
    }
}