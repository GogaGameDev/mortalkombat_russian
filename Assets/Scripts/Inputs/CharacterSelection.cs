// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Inputs/CharacterSelection.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @CharacterSelection : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @CharacterSelection()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""CharacterSelection"",
    ""maps"": [
        {
            ""name"": ""Selection"",
            ""id"": ""531719af-e92d-4a4c-8b08-82efd0bf5908"",
            ""actions"": [
                {
                    ""name"": ""Select"",
                    ""type"": ""Button"",
                    ""id"": ""7da19c02-1f98-4aee-ab82-b0316126ca34"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Deselect"",
                    ""type"": ""Button"",
                    ""id"": ""6855eb1f-20b0-4c69-8dc7-8fe143b88860"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Left"",
                    ""type"": ""Button"",
                    ""id"": ""d79a874f-3d7b-4005-a616-037308fac451"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right"",
                    ""type"": ""Button"",
                    ""id"": ""79d98b1c-2f75-44dd-bb4b-8e89892bd8fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Up"",
                    ""type"": ""Button"",
                    ""id"": ""222543ce-0cdc-44d9-b509-7d378d06826c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Down"",
                    ""type"": ""Button"",
                    ""id"": ""88db60e9-3333-4de2-a273-063637e6ef47"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7c867685-afe7-420e-a15a-23fdf7771184"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5286db98-aede-4672-b5aa-7f5275365911"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e22b04e8-5f32-47b0-b042-4cb953ab3bd4"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Deselect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a5170072-dec3-48e3-84ac-b04acb83f1d4"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Deselect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1e53cfea-a592-4982-ad5c-f1a9f0006e77"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25dda024-74e2-42aa-b450-771652aa95d8"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": ""Press,Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""91e6af9f-8e77-4b89-8758-f87192463074"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6a4a84d4-4950-4145-89b9-2935dc573ecd"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""69eca2d1-1192-4b27-9c74-a2462e1990fa"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": ""Press,Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b66d7a82-1e89-472d-929b-76a9c3687fa3"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""08a3658e-a72a-4454-b070-36f4d6925fb6"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4cf8b984-52a3-4cae-8d20-33f7bb983fdd"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae32bb4f-56a1-4cb7-aa6a-c9d64563da38"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4a488e0d-ce1b-4406-9c1c-3922f26ae0b2"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b71990ff-f5ad-4b73-8eba-e0a682ce507b"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""49016921-342b-4357-a83c-8a2b615d4437"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Ready"",
            ""id"": ""2f26cd81-b5d8-4f96-9639-f140602f91c4"",
            ""actions"": [
                {
                    ""name"": ""New action"",
                    ""type"": ""Button"",
                    ""id"": ""b8931e5b-ca4f-4eee-8a9b-e35b973562ff"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""8a817e81-2417-4e62-b32a-4b36d1a6e63c"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""New action"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Selection
        m_Selection = asset.FindActionMap("Selection", throwIfNotFound: true);
        m_Selection_Select = m_Selection.FindAction("Select", throwIfNotFound: true);
        m_Selection_Deselect = m_Selection.FindAction("Deselect", throwIfNotFound: true);
        m_Selection_Left = m_Selection.FindAction("Left", throwIfNotFound: true);
        m_Selection_Right = m_Selection.FindAction("Right", throwIfNotFound: true);
        m_Selection_Up = m_Selection.FindAction("Up", throwIfNotFound: true);
        m_Selection_Down = m_Selection.FindAction("Down", throwIfNotFound: true);
        // Ready
        m_Ready = asset.FindActionMap("Ready", throwIfNotFound: true);
        m_Ready_Newaction = m_Ready.FindAction("New action", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Selection
    private readonly InputActionMap m_Selection;
    private ISelectionActions m_SelectionActionsCallbackInterface;
    private readonly InputAction m_Selection_Select;
    private readonly InputAction m_Selection_Deselect;
    private readonly InputAction m_Selection_Left;
    private readonly InputAction m_Selection_Right;
    private readonly InputAction m_Selection_Up;
    private readonly InputAction m_Selection_Down;
    public struct SelectionActions
    {
        private @CharacterSelection m_Wrapper;
        public SelectionActions(@CharacterSelection wrapper) { m_Wrapper = wrapper; }
        public InputAction @Select => m_Wrapper.m_Selection_Select;
        public InputAction @Deselect => m_Wrapper.m_Selection_Deselect;
        public InputAction @Left => m_Wrapper.m_Selection_Left;
        public InputAction @Right => m_Wrapper.m_Selection_Right;
        public InputAction @Up => m_Wrapper.m_Selection_Up;
        public InputAction @Down => m_Wrapper.m_Selection_Down;
        public InputActionMap Get() { return m_Wrapper.m_Selection; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(SelectionActions set) { return set.Get(); }
        public void SetCallbacks(ISelectionActions instance)
        {
            if (m_Wrapper.m_SelectionActionsCallbackInterface != null)
            {
                @Select.started -= m_Wrapper.m_SelectionActionsCallbackInterface.OnSelect;
                @Select.performed -= m_Wrapper.m_SelectionActionsCallbackInterface.OnSelect;
                @Select.canceled -= m_Wrapper.m_SelectionActionsCallbackInterface.OnSelect;
                @Deselect.started -= m_Wrapper.m_SelectionActionsCallbackInterface.OnDeselect;
                @Deselect.performed -= m_Wrapper.m_SelectionActionsCallbackInterface.OnDeselect;
                @Deselect.canceled -= m_Wrapper.m_SelectionActionsCallbackInterface.OnDeselect;
                @Left.started -= m_Wrapper.m_SelectionActionsCallbackInterface.OnLeft;
                @Left.performed -= m_Wrapper.m_SelectionActionsCallbackInterface.OnLeft;
                @Left.canceled -= m_Wrapper.m_SelectionActionsCallbackInterface.OnLeft;
                @Right.started -= m_Wrapper.m_SelectionActionsCallbackInterface.OnRight;
                @Right.performed -= m_Wrapper.m_SelectionActionsCallbackInterface.OnRight;
                @Right.canceled -= m_Wrapper.m_SelectionActionsCallbackInterface.OnRight;
                @Up.started -= m_Wrapper.m_SelectionActionsCallbackInterface.OnUp;
                @Up.performed -= m_Wrapper.m_SelectionActionsCallbackInterface.OnUp;
                @Up.canceled -= m_Wrapper.m_SelectionActionsCallbackInterface.OnUp;
                @Down.started -= m_Wrapper.m_SelectionActionsCallbackInterface.OnDown;
                @Down.performed -= m_Wrapper.m_SelectionActionsCallbackInterface.OnDown;
                @Down.canceled -= m_Wrapper.m_SelectionActionsCallbackInterface.OnDown;
            }
            m_Wrapper.m_SelectionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Select.started += instance.OnSelect;
                @Select.performed += instance.OnSelect;
                @Select.canceled += instance.OnSelect;
                @Deselect.started += instance.OnDeselect;
                @Deselect.performed += instance.OnDeselect;
                @Deselect.canceled += instance.OnDeselect;
                @Left.started += instance.OnLeft;
                @Left.performed += instance.OnLeft;
                @Left.canceled += instance.OnLeft;
                @Right.started += instance.OnRight;
                @Right.performed += instance.OnRight;
                @Right.canceled += instance.OnRight;
                @Up.started += instance.OnUp;
                @Up.performed += instance.OnUp;
                @Up.canceled += instance.OnUp;
                @Down.started += instance.OnDown;
                @Down.performed += instance.OnDown;
                @Down.canceled += instance.OnDown;
            }
        }
    }
    public SelectionActions @Selection => new SelectionActions(this);

    // Ready
    private readonly InputActionMap m_Ready;
    private IReadyActions m_ReadyActionsCallbackInterface;
    private readonly InputAction m_Ready_Newaction;
    public struct ReadyActions
    {
        private @CharacterSelection m_Wrapper;
        public ReadyActions(@CharacterSelection wrapper) { m_Wrapper = wrapper; }
        public InputAction @Newaction => m_Wrapper.m_Ready_Newaction;
        public InputActionMap Get() { return m_Wrapper.m_Ready; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ReadyActions set) { return set.Get(); }
        public void SetCallbacks(IReadyActions instance)
        {
            if (m_Wrapper.m_ReadyActionsCallbackInterface != null)
            {
                @Newaction.started -= m_Wrapper.m_ReadyActionsCallbackInterface.OnNewaction;
                @Newaction.performed -= m_Wrapper.m_ReadyActionsCallbackInterface.OnNewaction;
                @Newaction.canceled -= m_Wrapper.m_ReadyActionsCallbackInterface.OnNewaction;
            }
            m_Wrapper.m_ReadyActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Newaction.started += instance.OnNewaction;
                @Newaction.performed += instance.OnNewaction;
                @Newaction.canceled += instance.OnNewaction;
            }
        }
    }
    public ReadyActions @Ready => new ReadyActions(this);
    public interface ISelectionActions
    {
        void OnSelect(InputAction.CallbackContext context);
        void OnDeselect(InputAction.CallbackContext context);
        void OnLeft(InputAction.CallbackContext context);
        void OnRight(InputAction.CallbackContext context);
        void OnUp(InputAction.CallbackContext context);
        void OnDown(InputAction.CallbackContext context);
    }
    public interface IReadyActions
    {
        void OnNewaction(InputAction.CallbackContext context);
    }
}