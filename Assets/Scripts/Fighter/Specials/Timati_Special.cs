using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class Timati_Special : SpecialBase
{
    [SerializeField] float _special_1_Damage = 10f;
    [SerializeField] float _special_2_Damage = 10f;
    [SerializeField] Projectile _projectile_1 = null;
    [SerializeField] Projectile _projectile_2 = null;

    GameObject projectileObj = null;


    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);

#if UNITY_EDITOR
        if (_fighter._playerID == 1)
        {
            // Fatality();
        }
#endif
    }

    #region Specials
    public override void Special_1(Vector3 spawnPos)
    {

#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif

        if (!CanDoSpecial)
        {
            return;
        }

        base.Special_1(spawnPos);

        Projectile _projectile = Instantiate<Projectile>(_projectile_1, spawnPos, Quaternion.identity);
        _projectile.InitThrow(_fighter.Inverted, _fighter);
        _projectile._OnHit.AddListener(OnHit_Special_1);
        projectileObj = _projectile.gameObject;

        //_combat.
    }

    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif


        if (!CanDoSpecial)
        {
            return;
        }
        base.Special_2(spawnPos);

        Projectile _projectile = Instantiate<Projectile>(_projectile_2, spawnPos, Quaternion.identity);
        _projectile.InitThrow(_fighter.Inverted, _fighter);
        _projectile._OnHit.AddListener(OnHit_Special_2);
        projectileObj = _projectile.gameObject;
    }

    void OnHit_Special_1(GameObject obj)
    {
        if (obj == this.gameObject)
        {
            return;
        }
        _combat.OnAttack.Invoke(_fighter._playerID, _special_1_Damage, AttackType.Special);
        if (projectileObj != null)
        {
            Destroy(projectileObj.gameObject, 0.25f);

        }
    }
    void OnHit_Special_2(GameObject obj)
    {
        if (obj == this.gameObject)
        {
            return;
        }
        _combat.OnAttack.Invoke(_fighter._playerID, _special_1_Damage, AttackType.Special);
        if (projectileObj != null)
        {
            Destroy(projectileObj.gameObject, 0.25f);

        }
    }


    #endregion /Specials
    #region Fatality
    [Space(10)]
    [Header("Fatality")]
    [SerializeField] SpriteRenderer foot;
    [SerializeField] GameObject bloodFXPrefab = null;
    public override void Fatality()
    {
        base.Fatality();
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }

    IEnumerator FatalityRoutine()
    {
        OnFatalityStarted.Invoke();
        yield return new WaitForSeconds(0.5f);
        Vector3 pos = _fighter.Opponent.position - (_fighter.Opponent.position - _fighter.transform.position).normalized * 1.8f;
        Direction dir = pos.x > transform.position.x ? Direction.Right : Direction.Left;
        bool backward = false;
        if ((dir == Direction.Right && _fighter.Inverted) || (dir == Direction.Left && !_fighter.Inverted))
        {
            backward = true;
        }
        _fighter.OnMoveStart.Invoke(backward);
        float moveDelay = Mathf.Abs(_fighter.transform.position.x - pos.x) / 1.1f;
        _fighter.transform.DOMove(pos, moveDelay);
        yield return new WaitForSeconds(moveDelay);
        _fighter.OnMoveStop.Invoke();
        yield return new WaitForSeconds(1f);
        foot.flipX = _fighter.Inverted;
        Vector3 endPos = _fighter.Opponent.transform.position + new Vector3(0, -1.4f, 0);
        Vector3 startPos = endPos + transform.up * 3f;
        foot.transform.position = startPos;
        foot.gameObject.SetActive(true);
        foot.transform.DOMove(endPos, 0.15f);
        yield return new WaitForSeconds(0.05f);
        _fighter.Opponent.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.125f);
        Instantiate(bloodFXPrefab, _fighter.Opponent.position + new Vector3(0, -1.25f, 0), Quaternion.identity);
        foot.transform.DOShakePosition(0.2f, Vector3.right * 0.25f, 10, 0);
        yield return new WaitForSeconds(0.3f);
        OnFatalityFinished.Invoke();

    }

    #endregion /Fatality
}