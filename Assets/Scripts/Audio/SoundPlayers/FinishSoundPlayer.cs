﻿using UnityEngine;

namespace Audio.SoundPlayers
{
    /// <summary>
    /// Provides a method for playing "Finish him" or "Finish her" sound
    /// </summary>
    public class FinishSoundPlayer : SoundPlayer
    {
        [SerializeField] private AudioClip _finishHim;
        [SerializeField] private AudioClip _finishHer;

        /// <summary>
        /// Use it before playback to set gender
        /// </summary>
        public void SetGender(Gender gender)
        {
            _gender = gender;
        }

        public override void Play()
        {
            PlayOnce(_gender switch
            {
                Gender.Male => _finishHim,
                Gender.Female => _finishHer,
                _ => _finishHim
            });
        }

        private Gender _gender = Gender.Male;
    }
}