﻿using UnityEngine;

namespace Audio.SoundPlayers
{
    /// <summary>
    /// Provides a method for playing random sound from the AudioClip array
    /// </summary>
    public class RandomSoundPlayer : SoundPlayer
    {
        [SerializeField] private AudioClip[] _audioClips;

        public override void Play()
        {
            PlayOnce(_audioClips[Random.Range(0, _audioClips.Length)]);
        }
    }
}