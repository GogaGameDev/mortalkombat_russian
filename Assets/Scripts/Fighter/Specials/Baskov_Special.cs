using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class Baskov_Special : SpecialBase
{
    [SerializeField] float _special_1_Damage = 10f;
    [SerializeField] float _special_2_Damage = 10f;
    [SerializeField] Projectile _projectile_1 = null;
    [SerializeField] GameObject _screamObj = null;

    GameObject projectileObj = null;


    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);

#if UNITY_EDITOR
        if (_fighter._playerID == 1)
        {
            // Fatality();
        }
#endif
    }

    #region Specials

    public override void Special_1(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif


        if (!CanDoSpecial) {
            return;
        }
        base.Special_1(spawnPos);

        Projectile _projectile = Instantiate<Projectile>(_projectile_1, spawnPos, Quaternion.identity);
        _projectile.InitThrow(_fighter.Inverted, _fighter);
        _projectile._OnHit.AddListener(OnHit_Special_1);
        projectileObj = _projectile.gameObject;

        //_combat.
    }

    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif


        if (!CanDoSpecial)
        {
            return;
        }

        base.Special_2(spawnPos);

        StartCoroutine(Special_2Routine(spawnPos));
    }
    IEnumerator Special_2Routine(Vector3 spawnPos)
    {
        spawnPos = _fighter.transform.position + (_fighter.Inverted ? -transform.right : transform.right) * 0.8f + new Vector3(0, -0.25f, 0);
         GameObject screamObj = Instantiate(_screamObj, spawnPos, Quaternion.identity);
        screamObj.GetComponent<SpriteRenderer>().flipX = _fighter.Inverted;
        Destroy(screamObj, 1f);
        Vector3 origin = spawnPos + (_fighter.Inverted ? new Vector3(0.5f, 0, 0) : new Vector3(-0.5f, 0, 0));
        Vector3 direction = _fighter.Inverted ? -transform.right : transform.right;
        float distance = 0.8f;
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Debug.DrawRay(origin, direction.normalized * distance, Color.red, 3f);
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, mask);

        if (hit.collider != null)
        {
            Fighter f = hit.collider.gameObject.GetComponent<Fighter>();

            if (f.TakeDamage(_special_2_Damage, AttackType.Special) == 0)
            {
                yield break;
            }
            f.Inputs.ToggleControls(false);
            f.ResetAllActiveStatus();
            yield return new WaitForSeconds(0.1f);
            f.OnHit.Invoke(HitType.Mid);
            yield return new WaitForSeconds(0.1f);
            f.gameObject.GetComponent<Animator>().enabled = false;

            if (f.transform.position.x != 0.159f)
            {
                f.transform.position = new Vector3(f.transform.position.x, 0.159f, f.transform.position.z);
            }
            f.transform.DOKill();
            yield return new WaitForSeconds(0.1f);
            // f.transform.DOShakePosition(1.25f, Vector3.right*0.1f, 8,0);
            f.transform.DOMoveX(f.transform.position.x + 0.1f, 0.075f).SetLoops(-1, LoopType.Yoyo);
            yield return new WaitForSeconds(1f);
            f.transform.DOKill();
            f.gameObject.GetComponent<Animator>().enabled = true;
            yield return new WaitForSeconds(0.25f);
            f.Inputs.ToggleControls(true);



        }

    }
    void OnHit_Special_1(GameObject obj)
    {
        if (obj == this.gameObject)
        {
            return;
        }
        _combat.OnAttack.Invoke(_fighter._playerID, _special_1_Damage, AttackType.Special);
        if (projectileObj != null)
        {
            Destroy(projectileObj.gameObject, 0.25f);

        }
    }



    #endregion /Specials
    #region Fatality
    [Space(10)]
    [Header("Fatality")]
    [SerializeField] SpriteMask _mask;
    [SerializeField] GameObject _machine;
    [SerializeField] GameObject bodyPartsPrefab;
    [SerializeField] GameObject bloodPrefab;
    bool fatalityShowed = false;
    public override void Fatality()
    {
        base.Fatality();
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }

    IEnumerator FatalityRoutine()
    {
        OnFatalityStarted.Invoke();
        Fighter opponent = _fighter.Opponent.gameObject.GetComponent<Fighter>();
        fatalityShowed = true;
        yield return new WaitForSeconds(0.5f);
        //set positions
        _mask.transform.localPosition = new Vector3(_fighter.Inverted ? -_mask.transform.localPosition.x : _mask.transform.localPosition.x, _mask.transform.localPosition.y, _mask.transform.localPosition.z);
        _machine.transform.localPosition = new Vector3(_fighter.Inverted ? -_machine.transform.localPosition.x : _machine.transform.localPosition.x, _machine.transform.localPosition.y, _machine.transform.localPosition.z);
        opponent.RB.constraints = RigidbodyConstraints2D.FreezeAll;
        opponent.UpdateColiderEnable(false);
        Vector3 pos = _fighter.Opponent.position - (_fighter.Opponent.position - _fighter.transform.position).normalized * 0.5f;
        _fighter.OnMoveStart.Invoke(_fighter.Inverted);
        _fighter.SetSortLayer(10);
        float moveDelay = Mathf.Abs(_fighter.transform.position.x - pos.x) / 1.1f;
        _fighter.RB.constraints = RigidbodyConstraints2D.FreezeAll;
        _fighter.transform.DOMove(pos, moveDelay);
        yield return new WaitForSeconds(moveDelay);
        _fighter.OnMoveStop.Invoke();
        _fighter.Animation.SetBool("hand_attack_bool", true);
        _fighter.Animation.SetBool("hand_attack_uppercut_bool", true);
        _fighter.Animation.SetTrigger("attack_state_trigger");
        yield return new WaitForSeconds(0.25f);
        _fighter.Opponent.gameObject.GetComponent<Animator>().SetTrigger("fall_trigger");
        yield return new WaitForSeconds(0.1f);
        _fighter.Opponent.gameObject.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        _fighter.Opponent.transform.DOMoveY(_fighter.Opponent.transform.position.y + 5f, 1f);
        yield return new WaitForSeconds(1f);
        _fighter.Opponent.gameObject.GetComponent<Animator>().SetTrigger("baskov_fatality_reaction_trigger");
        _fighter.Animation.SetTrigger("fatality_trigger");
        _mask.gameObject.SetActive(true);
        _fighter.Opponent.transform.DOMoveY(_fighter.Opponent.transform.position.y - 4f, 0.75f);
        yield return new WaitForSeconds(1f);
        _fighter.Opponent.transform.DOMoveY(_fighter.Opponent.transform.position.y - 5f, 5f);
        yield return new WaitForSeconds(1f);
        Instantiate(bodyPartsPrefab, _machine.transform.position + new Vector3(0,-0.2f,0), Quaternion.identity);
        Instantiate(bloodPrefab, _machine.transform.position + new Vector3(0, -0.2f, 0), Quaternion.identity);
        yield return new WaitForSeconds(1f);
        OnFatalityFinished.Invoke();

    }
    public void ShowMachine()
    {
        if (fatalityShowed)
        {
            _machine.transform.localScale = new Vector3(_fighter.Inverted ? -_machine.transform.localScale.x : _machine.transform.localScale.x,_machine.transform.localScale.y,_machine.transform.localScale.z);
            _machine.gameObject.SetActive(true);
        }
    }
    #endregion /Fatality
}