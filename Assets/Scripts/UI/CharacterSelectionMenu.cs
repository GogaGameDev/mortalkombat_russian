using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;
using TMPro;
public class CharacterSelectionMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI[] P_names;
    [SerializeField] FighterList _list;
    [SerializeField] MatchParams _params;
    [SerializeField] List<FighterDisplay> _fighterDisplayList;
    [SerializeField] FighterDisplay[] pFighters = new FighterDisplay[2];
    [SerializeField] Transform[] _playerPositions;
    [SerializeField] List<FighterUISelection> _allFighterP1 = new List<FighterUISelection>();
    [SerializeField] List<FighterUISelection> _allFighterP2 = new List<FighterUISelection>();
    Coroutine _randomRoutineP1 = null;
    Coroutine _randomRoutineP2 = null;
    [SerializeField] bool[] playerChoose;
    void Start()
    {
        Init();
    }

    void Init()
    {
        playerChoose = new bool[2];
        for (int j = 0; j < _fighterDisplayList.Count; j++)
        {

            if (!_fighterDisplayList[j].Random)
            {

                _fighterDisplayList[j].Init(_list._fighters[j]);
                FighterUISelection fUI_1 = Instantiate<FighterUISelection>(_fighterDisplayList[j]._fighterPrefabUI, _playerPositions[0]);
                FighterUISelection fUI_2 = Instantiate<FighterUISelection>(_fighterDisplayList[j]._fighterPrefabUI, _playerPositions[1]);
                fUI_1.transform.localPosition = Vector3.zero;
                fUI_1.Init(1, _list._fighters[j]._fighterID);
                _allFighterP1.Add(fUI_1);
                fUI_2.transform.localPosition = Vector3.zero;
                fUI_2.Init(2, _list._fighters[j]._fighterID);
                _allFighterP2.Add(fUI_2);
            }

            _fighterDisplayList[j].OnHighLight.AddListener(InstantiateFighter);


        }

        pFighters[0].HighLight(1, true);
        pFighters[1].HighLight(2, true);
        UpdateNames();
    }

    void UpdateNames()
    {
        P_names[0].text = pFighters[0].HideName && !pFighters[0]._isSelected?"" : pFighters[0].FighterID;
        P_names[1].text = pFighters[1].HideName &&  !pFighters[1]._isSelected ? "":pFighters[1].FighterID;

    }
    void UpdateName(int playerID, string name)
    {   
        P_names[playerID].text = name;

    }
    public void MoveOnGrid(Direction dir, int playerID)
    {
        //Debug.Log("MoveOnGrid "+ playerID+" player");
        Vector2 nextPos = Vector2.zero;
        FighterDisplay oldDisplay = pFighters[playerID - 1];
        if (oldDisplay._isSelected && oldDisplay._playerID == playerID)
        {

            return;
        }
        nextPos = oldDisplay._positionInList;
        FighterDisplay newDisplay = null;
        switch (dir)
        {
            case Direction.Left:
                nextPos.y--;
                //  newDisplay = _fighterDisplayList.FirstOrDefault(x => x._positionInList.x == nextPos.x && x._positionInList.y == nextPos.y);
                //if (newDisplay != null && newDisplay.IsHighlighted && !newDisplay.Random)
                //{
                //    nextPos.y--;
                //}
                break;
            case Direction.Right:
                nextPos.y++;
                //newDisplay = _fighterDisplayList.FirstOrDefault(x => x._positionInList.x == nextPos.x && x._positionInList.y == nextPos.y);
                //if (newDisplay != null && newDisplay.IsHighlighted && !newDisplay.Random)
                //{
                //    nextPos.y++;
                //}
                break;
            case Direction.Up:
                nextPos.x--;
                //newDisplay = _fighterDisplayList.FirstOrDefault(x => x._positionInList.x == nextPos.x && x._positionInList.y == nextPos.y);
                //if (newDisplay != null && newDisplay.IsHighlighted && !newDisplay.Random)
                //{
                //    nextPos.x--;
                //}
                break;
            case Direction.Down:
                nextPos.x++;
                //newDisplay = _fighterDisplayList.FirstOrDefault(x => x._positionInList.x == nextPos.x && x._positionInList.y == nextPos.y);
                //if (newDisplay != null && newDisplay.IsHighlighted && !newDisplay.Random)
                //{
                //    nextPos.x++;
                //}
                break;
        }

        // Debug.Log("new pos " + nextPos);

        newDisplay = _fighterDisplayList.FirstOrDefault(x => x._positionInList.x == nextPos.x && x._positionInList.y == nextPos.y);
        if (newDisplay == null)
        {
            //Debug.LogWarning("cannot move to " + nextPos);
            return;
        }
        pFighters[playerID - 1] = newDisplay;
        oldDisplay.HighLight(playerID, false);
        newDisplay.HighLight(playerID, true);
        UpdateNames();
    }


    public void Select(int playerID)
    {
        if (pFighters[playerID - 1].Random)
        {
            pFighters[playerID - 1].HighLight(playerID, false);
            pFighters[playerID - 1] = GetRandomFighter(playerID);

        }

        if (pFighters[playerID - 1]._isSelected)
        {
            // return;
        }

        pFighters[playerID - 1].Select(playerID);
        playerChoose[playerID - 1] = true;
        FighterUISelection selectedFighter = playerID == 1 ?
            _allFighterP1.FirstOrDefault(x => x.FighterID == pFighters[playerID - 1].FighterID) :
            _allFighterP2.FirstOrDefault(x => x.FighterID == pFighters[playerID - 1].FighterID);
        if (selectedFighter == null)
        {
            Debug.Log("No selectedFighter...");
        }
        selectedFighter.Select();
    

        if (playerChoose[0] && playerChoose[1])
        {
            StartFight();
        }
        UpdateNames();
    }
    FighterDisplay GetRandomFighter(int playerID)
    {
        FighterDisplay f = null;
        while (f == null || f._isSelected || f.Random)
        {
            int id = Random.Range(0, _fighterDisplayList.Count);
            f = _fighterDisplayList[id];
        }
        return f;
    }
    void StartFight()
    {
        Debug.LogWarning("Loading Fight Scene...");
        _params.player_FighterPrefab[0] = pFighters[0]._fighterPrefab_Ingame;
        _params.player_FighterPrefab[1] = pFighters[1]._fighterPrefab_Ingame;
        StartCoroutine(LoadScene(1f));

    }
    IEnumerator LoadScene(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("Main");
    }
    public void Deselect(int playerID)
    {
        if (!pFighters[playerID - 1]._isSelected)
        {
            return;
        }
        pFighters[playerID - 1].Deselect();
        playerChoose[playerID - 1] = false;
    }

    public void InstantiateFighter(int playerID, bool random = false)
    {
        List<FighterUISelection> fList = playerID == 1 ? _allFighterP1 : _allFighterP2;

        foreach (var item in fList)
        {
            item.gameObject.SetActive(false);
        }

        Coroutine routine = playerID == 1 ? _randomRoutineP1 : _randomRoutineP2;
        if (routine != null)
        {
            StopCoroutine(routine);
        }
        if (random)
        {
            switch (playerID)
            {
                case 1:
                    _randomRoutineP1 = StartCoroutine(RandomRoutine(fList, playerID));
                    break;
                case 2:
                    _randomRoutineP2 = StartCoroutine(RandomRoutine(fList, playerID));
                    break;
            }
            return;

        }

        FighterUISelection selectedF = fList.FirstOrDefault(x => x.FighterID == pFighters[playerID - 1].FighterID);
        if (selectedF != null)
        {
            selectedF.gameObject.SetActive(true);
        }
        UpdateNames();
    }

    IEnumerator RandomRoutine(List<FighterUISelection> list, int playerID)
    {

        yield return new WaitForEndOfFrame();

        while (!pFighters[playerID - 1]._isSelected)
        {
            foreach (var item in list)
            {
                item.gameObject.SetActive(true);
               
                UpdateName(playerID - 1, item.HideName?"":item.FighterID);
                yield return new WaitForSeconds(0.2f);
                item.gameObject.SetActive(false);
            }

        }
    }
}