using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class BoyarskySpecial : SpecialBase
{
    [Header("special-1")]
    [SerializeField] float _special_1_Damage = 10f;
    [SerializeField] Projectile _hatPrefab = null;
    GameObject projectileObj = null;
    [Header("special-2")]
    [SerializeField] float _special_2_Damage = 10f;
    [SerializeField] Transform _scarfStartTransformPosition = null;
    [SerializeField] Vector3 _startLineOffset = Vector3.zero;
    [SerializeField] LineRenderer _scarfLine = null;
    [SerializeField] Transform _scarfEndTransform = null;
    float _scarfLengthMax = 1.5f;
    [SerializeField] Material[] positionMaterialScarf;
    Transform _opponentTransform = null;
    [Header("Fatality")]
    [SerializeField] GameObject _devilsContainer = null;
    [SerializeField] Animator[] _devilAnimators;
    [SerializeField] Coroutine special_2_Routine = null;
    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (_fighter._playerID == 1 && Input.GetKeyDown(KeyCode.X))
        {
           // Fatality();
        }
#endif
    }
    #region Specials
    public override void Special_1(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif

        if (!CanDoSpecial) 
        {
            return;
        }
        base.Special_1(spawnPos);

        Projectile hat = Instantiate<Projectile>(_hatPrefab, spawnPos, Quaternion.identity);
        hat.InitThrow(_fighter.Inverted, _fighter);
        hat._OnHit.AddListener(OnHit_Special_1);
        projectileObj = hat.gameObject;

        //_combat.
    }

    public override void ResetSpecials()
    {
        if (special_2_Routine != null)
        {
            StopCoroutine(special_2_Routine);
            ResetSpecial_2();

        }
    }
    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif


        if (!CanDoSpecial)
        {
            return;
        }

        base.Special_2(spawnPos);
        if (special_2_Routine != null)
        {
            StopCoroutine(special_2_Routine);
        }
        special_2_Routine = StartCoroutine(Special_2Routine());

    }

    IEnumerator Special_2Routine()
    {

        _fighter.Animation.SetBool("individual_special_bool", true);
        _startLineOffset = new Vector3(_fighter.Inverted ? -0.05f : 0.05f, -0.02f, 0f);
        Fighter _opponent = null;
        _opponentTransform = null;
        _scarfLine.material = _fighter.Inverted ? positionMaterialScarf[0] : positionMaterialScarf[1];
        float distanceBetween = 10f;
        float minDistance = 1.2f;
        float endScarfXOffset = _fighter.Inverted ? 0.11f : -0.11f;


        int previosSortingOrder = _fighter.SetSortLayer(5);
        Vector3 scarfStarPos = new Vector3(_scarfStartTransformPosition.position.x, -0.245f, _scarfStartTransformPosition.position.z);

        if (!_fighter.Inverted)
        {
            scarfStarPos = new Vector3(transform.position.x + (transform.position.x - _scarfStartTransformPosition.position.x), scarfStarPos.y, _scarfStartTransformPosition.position.z);

        }
        //detect if hit the opponent
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Vector3 direction = _fighter.Inverted ? -_fighter.transform.right : _fighter.transform.right;
        Vector3 originPosOffset = new Vector3(-0.65f, 0, 0);
        Vector3 originPos = scarfStarPos + (!_fighter.Inverted ? originPosOffset : -originPosOffset);
        Vector3 fixedOriginPos = originPos - direction.normalized * 0.5f;
        RaycastHit2D hit = Physics2D.Raycast(fixedOriginPos, direction, _scarfLengthMax * 1.25f, mask);
        Debug.DrawRay(fixedOriginPos, direction * _scarfLengthMax * 1.25f, Color.red, 1f);

        if (hit.collider != null)
        {
            Debug.LogWarning(hit.collider.gameObject.name + " hitted");
            _opponent = hit.collider.gameObject.GetComponent<Fighter>();
            _opponentTransform = _opponent.transform;

            distanceBetween = Mathf.Abs(this.transform.position.x - hit.collider.gameObject.transform.position.x);

            bool success = false;
            if (_opponent.Health == 0)
            {
                success = true;
            }
            else
            {
                success = _opponent.TakeDamage(_special_2_Damage, AttackType.Special) != 0;
            }
            if (success)
            {


                _opponent.Inputs.ToggleControls(false);
                _opponent.ResetAllActiveStatus();
                yield return new WaitForSeconds(0.1f);
                _fighter.IsDoingSpecial = true;
                _opponent.OnHit.Invoke(HitType.Mid);
                yield return new WaitForSeconds(0.1f);
                _opponent.GetComponent<Animator>().enabled = false;
                if (_opponent.Health == 0)
                {
                    _opponent.TakeDamage(_special_2_Damage, AttackType.Special);
                }
            }
            else
            {
                _opponent = null;
            }
        }
        //

        // set line start
        Vector3[] linePositions = new Vector3[2];
        if (distanceBetween > minDistance)
        {
            _fighter.Animation.SetTrigger("special_2_end_start_trigger");
            linePositions[0] = scarfStarPos + _startLineOffset;
            float length = hit.collider == null ? _scarfLengthMax * 0.4f : Mathf.Abs(hit.transform.position.x - originPos.x) * 0.5f;
            //set line end
            linePositions[1] = new Vector3(linePositions[0].x, linePositions[0].y, linePositions[0].z) + direction * length;
            _scarfLine.positionCount = 2;
            _scarfLine.SetPositions(linePositions);
            _scarfLine.gameObject.SetActive(true);
        }

        if (distanceBetween < 0.29f)
        {
            Vector3 dir = _fighter.Inverted ? transform.right : -transform.right;
            RaycastHit2D border = Physics2D.Raycast(transform.position, dir, 1f, 1 << LayerMask.NameToLayer("Border"));
            Debug.DrawRay(transform.position, dir.normalized * 1f, Color.red, 2f);
            if (border.collider == null)
            {
                Vector3 newPos = transform.position + dir * 0.5f;
                transform.DOMove(newPos, 0.1f);

            }
            else
            {
                Vector3 newPos = _opponent.transform.position - dir * 0.5f;
                _opponent.transform.DOMove(newPos, 0.1f);
            }

            yield return new WaitForSeconds(0.1f);
        }

        //set scarf end
        _scarfEndTransform.position = new Vector3(linePositions[1].x + endScarfXOffset, _scarfEndTransform.position.y, linePositions[1].z-0.05f);
        if (hit.collider != null)
        {
            _scarfEndTransform.position = new Vector3(hit.collider.transform.position.x + endScarfXOffset, _scarfEndTransform.position.y, linePositions[1].z-0.05f);
        }

        _scarfEndTransform.localScale = new Vector3(_fighter.Inverted ? 1.1f : -1.1f, 1f, 1f);
        _scarfEndTransform.gameObject.SetActive(true);


        yield return new WaitForSeconds(0.5f);
        //pull
        if (_opponent == null)
        {
            // yield return new WaitForSeconds(0.1f);
            ResetSpecial_2();
            yield break;
        }
        if (distanceBetween < minDistance)
        {
            Debug.Log("distanceBetween :" + distanceBetween);
            _scarfEndTransform.localScale = new Vector3(_fighter.Inverted ? 1.4f : -1.4f, 1f, 1f);
            _opponent.transform.position = this.transform.position + transform.right * (_fighter.Inverted ? -0.8203534f : 0.8203534f);
            _scarfEndTransform.position = new Vector3(_opponent.transform.position.x + endScarfXOffset * 2.5f, _scarfStartTransformPosition.position.y - 0.05f, _scarfEndTransform.position.z);
            Debug.Log(Mathf.Abs(this.transform.position.x - _opponent.transform.position.x));
            _fighter.Animation.SetTrigger("special_2_mid_trigger");
            _scarfLine.positionCount = 0;

            //correct boyarsky pos

            yield return new WaitForSeconds(0.75f);

        }
        else
        {
            _fighter.Animation.SetTrigger("special_2_end_trigger");

            yield return new WaitForSeconds(0.75f);
        }
        ResetSpecial_2();
        if (_opponent != null)
        {
            _opponent.GetComponent<Animator>().enabled = true;

            yield return new WaitForSeconds(0.1f);
            if (_opponent.Health > 0)
            {
                _opponent.Stunn(1f);
            }
            else
            {
                _opponent.Animation.SetTrigger("stunn_trigger");
                _opponent.Animation.SetTrigger("final_death_trigger");
                _fighter.IsDoingSpecial = false;
            }

        }

        _fighter.SetSortLayer(previosSortingOrder);

    }

    public void MoveScarf(float moveValue)
    {
        moveValue = _fighter.Inverted ? moveValue : -moveValue;
        _scarfEndTransform.transform.position += transform.right * moveValue;
        _scarfLine.SetPosition(0, _scarfLine.GetPosition(0) + transform.right * moveValue);
        _scarfLine.SetPosition(1, _scarfLine.GetPosition(1) + transform.right * moveValue);

        if (_opponentTransform != null)
        {
            _opponentTransform.transform.position += transform.right * moveValue;
        }

    }
    void ResetSpecial_2()
    {
        _scarfEndTransform.gameObject.SetActive(false);
        _scarfLine.gameObject.SetActive(false);
        _fighter.Animation.SetBool("individual_special_bool", false);
        _fighter.Animation.SetTrigger("IDL");
        _fighter.IsDoingSpecial = false;
    }
    void OnHit_Special_1(GameObject obj)
    {
        if (obj == this.gameObject)
        {
            return;
        }
        _combat.OnAttack.Invoke(_fighter._playerID, _special_1_Damage, AttackType.Special);
        if (projectileObj != null)
        {
            Destroy(projectileObj.gameObject, 0.25f);

        }
    }



    #endregion /Specials
    #region Fatality
    [SerializeField] GameObject _bloodFXPrefab = null;
    [SerializeField] GameObject _bloodMediumPrefab= null;

    public override void Fatality()
    {
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }
    IEnumerator FatalityRoutine()
    {

        OnFatalityStarted.Invoke();
        yield return new WaitForSeconds(0.5f);
        _fighter.Animation.SetState("fatality_pose");
        _devilsContainer.gameObject.SetActive(true);
        _devilsContainer.transform.localScale = !_fighter.Inverted ? new Vector3(-1f, 1f, 1f) : new Vector3(1f, 1f, 1f);
        Vector3 direction = _fighter.Inverted ? -_fighter.transform.right : _fighter.transform.right;
        _devilsContainer.transform.localPosition = (!_fighter.Inverted ? new Vector3(-2.5f, 0, 0) : new Vector3(2.5f, 0, 0));

        foreach (var devil in _devilAnimators)
        {
            float distance = Vector3.Distance(devil.transform.position, _fighter.transform.position) + Random.Range(5f, 7f);
            float duration = Random.Range(4f, 5.5f);
            Vector3 endPos = devil.transform.position + direction * distance;
            devil.enabled = true;
            devil.transform.DOMove(endPos, duration);

            yield return new WaitForSeconds(0.1f);
        }


        _fighter.Opponent.gameObject.GetComponent<Fighter>().Animation.SetState("FinalDeath");
        yield return new WaitForSeconds(0.5f);
        Instantiate(_bloodFXPrefab, _fighter.Opponent.transform.position + new Vector3(Random.Range(-0.35f,0.35f), -1.15f, 0), Quaternion.identity);
        yield return new WaitForSeconds(0.3f);
        Instantiate(_bloodFXPrefab, _fighter.Opponent.transform.position + new Vector3(Random.Range(-0.35f, 0.35f), -1.15f, 0), Quaternion.identity);
        yield return new WaitForSeconds(0.3f);
        Instantiate(_bloodFXPrefab, _fighter.Opponent.transform.position + new Vector3(0, -1.15f, 0), Quaternion.identity);
        _fighter.Opponent.gameObject.SetActive(false);
        Instantiate(_bloodMediumPrefab, _fighter.Opponent.transform.position + new Vector3(0, -1.3f, 0), Quaternion.identity);
        yield return new WaitForSeconds(2.3f);
        OnFatalityFinished.Invoke();
    }
    #endregion /Fatality
}