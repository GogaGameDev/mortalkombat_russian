using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class FighterDisplay : MonoBehaviour
{

    public int _playerID = -1;
    [SerializeField] GameObject[] _p1SelectionObjs;
    [SerializeField] Image[] pBackgroundImages;
    [SerializeField] Sprite[] pBackgroundSprite;
    [SerializeField] GameObject[] _p2SelectionObjs;
    [SerializeField] Image _fighterIcon_Image;
    [SerializeField] TextMeshProUGUI _playerID_Text;
    [SerializeField] string _fighterDisplayName;
    [SerializeField] string _fighterID;
    [SerializeField] bool _isHighiighted = false;
    [SerializeField] bool _randomFlag = false;
    public bool Random => _randomFlag;
    public Vector2 _positionInList;
    public bool _isSelected = false;
    public string FighterID => _fighterID;
    public bool IsHighlighted => _isHighiighted;
    [HideInInspector] public EventIntBool OnHighLight = new EventIntBool();
    public FighterUISelection _fighterPrefabUI;
    public Fighter _fighterPrefab_Ingame;
    public bool HideName;
    Animator _animator = null;
    // Start is called before the first frame update
    public UnityEvent OnSelect;


    private void Start()
    {
        _animator = this.GetComponent<Animator>();
    }
    public void Select(int playerID)
    {
        if (!_isHighiighted)
        {
            HighLight(playerID, true);
        }
        _isSelected = true;
        _playerID = playerID;
        _animator.SetTrigger("reset_trigger");
        OnSelect.Invoke();


    }
    public void Deselect()
    {

        HighLight(_playerID, true);
        pBackgroundImages[_playerID - 1].sprite = pBackgroundSprite[_playerID - 1];
        _isSelected = false;
        _playerID = -1;


    }
    public void HighLight(int playerID, bool lightOn)
    {
        _isHighiighted = lightOn;
        if (_animator == null)
        {
            _animator = this.GetComponent<Animator>();
        }
        if (!lightOn)
        {
            if (playerID == 1) HighLightUpdate(_p1SelectionObjs, false);
            if (playerID == 2) HighLightUpdate(_p2SelectionObjs, false);
            return;
        }

        if (playerID == 1) { HighLightUpdate(_p1SelectionObjs, true); _animator.SetTrigger("select_p1_trigger"); };
        if (playerID == 2) { HighLightUpdate(_p2SelectionObjs, true); _animator.SetTrigger("select_p2_trigger"); };

        OnHighLight.Invoke(playerID, _randomFlag);

    }
    void HighLightUpdate(GameObject[] selections, bool activate)
    {
        if (!Random)
        {
            selections[0].gameObject.SetActive(activate);

        }
        selections[1].gameObject.SetActive(activate);
    }
    public void Init(FighterInLine fighter, Vector2 posInGrid)
    {

        _fighterIcon_Image.sprite = fighter._fighterIcon;
        _fighterDisplayName = fighter._fighterDisplayName;
        _positionInList = posInGrid;
        _playerID_Text.gameObject.SetActive(false);
        _fighterPrefabUI = fighter._fighterPrefab;
        _fighterPrefab_Ingame = fighter._ingameFighterPrefab;
        _fighterID = fighter._fighterID;

        for (int i = 0; i < 2; i++)
        {
            _p1SelectionObjs[i].gameObject.SetActive(false);
            _p2SelectionObjs[i].gameObject.SetActive(false);
        }
    }
    public void Init(FighterInLine fighter)
    {
        _fighterIcon_Image.sprite = fighter._fighterIcon;
        _fighterDisplayName = fighter._fighterDisplayName;
        //_positionInList = posInGrid;
        HideName = fighter.hideName;
        _playerID_Text.gameObject.SetActive(false);
        _fighterPrefabUI = fighter._fighterPrefab;
        _fighterPrefab_Ingame = fighter._ingameFighterPrefab;
        _fighterID = fighter._fighterID;
        for (int i = 0; i < 2; i++)
        {
            _p1SelectionObjs[i].gameObject.SetActive(false);
            _p2SelectionObjs[i].gameObject.SetActive(false);
        }
    }
}