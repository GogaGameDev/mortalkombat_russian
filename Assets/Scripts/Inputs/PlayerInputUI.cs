using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class PlayerInputUI : MonoBehaviour
{
    [SerializeField] int _playerID;
    CharacterSelection _inputs = null;

    public EventDirectionInt OnMove = new EventDirectionInt();
    public EventInt OnSelect = new EventInt();
    public EventInt OnDeselect = new EventInt();

    void Awake()
    {


        _inputs = new CharacterSelection();


        _inputs.Selection.Select.performed += ctx =>
        {


            //Debug.Log("Select"); 
            OnSelect.Invoke(_playerID);

        };
        _inputs.Selection.Deselect.performed += ctx => { OnDeselect.Invoke(_playerID); };
        _inputs.Selection.Left.performed += ctx => { OnMove.Invoke(Direction.Left, _playerID); };
        _inputs.Selection.Right.performed += ctx => { OnMove.Invoke(Direction.Right, _playerID); };
        _inputs.Selection.Up.performed += ctx => { OnMove.Invoke(Direction.Up, _playerID); };
        _inputs.Selection.Down.performed += ctx => { OnMove.Invoke(Direction.Down, _playerID); };

        if (_playerID == 1)
        {
            _inputs.devices = new[] { Gamepad.all.Count == 0 ? InputSystem.devices[0] : Gamepad.all[0] };
        }
        if (_playerID == 2)
        {
            _inputs.devices = new[] { Gamepad.all.Count > 1 ? Gamepad.all[1] : (Gamepad.all.Count > 0 ? InputSystem.devices[0] : null) };
        }
    }

    private void OnEnable()
    {
        _inputs.Enable();
    }
    private void OnDisable()
    {
        _inputs.Disable();
    }

}