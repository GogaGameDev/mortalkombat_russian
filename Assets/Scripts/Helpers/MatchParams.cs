using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Match Params", menuName = "Game/Match Params")]

public class MatchParams : ScriptableObject
{
    public Fighter[] player_FighterPrefab = new Fighter[2];
    public int roundCount = 2;
    public int timer = 60;
    public GameObject arenaPrefab;

}