﻿using UnityEngine;

namespace Audio
{
    public class OnRoundStartPlay : OnEventPlay
    {
        [SerializeField] private GameManager _gameManager;
        protected override void SubscribeToEvent()
        {
            _gameManager.OnRestart.AddListener(PlaySound);
        }
    }
}