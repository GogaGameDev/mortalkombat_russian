using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class Garaev_Special : SpecialBase
{
    [SerializeField] float _special_1_Damage = 10f;
    [SerializeField] float _special_2_Damage = 10f;
    Coroutine _special_1_Routine=null;
    Coroutine _special_2_Routine = null;
    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);

#if UNITY_EDITOR
        if (_fighter._playerID == 1)
        {
            // Fatality();
        }
#endif

    }

    #region Specials
    public override void ResetSpecials()
    {
        base.ResetSpecials();
        if (_special_1_Routine != null) 
        {
         
             StopCoroutine(_special_1_Routine);
            _special_1_Routine = null;
            _fighter.Combat.Air_leg_coldier.gameObject.SetActive(true);
            _fighter.UpdateColiderEnable(true);
        }
    }
    public override void Special_1(Vector3 spawnPos)
    {

#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif

        if (!_canDoSpecial) {
            return;
        }
        base.Special_1(spawnPos);
        if (_special_1_Routine != null)
        {
            return;
        }
        _special_1_Routine= StartCoroutine(Special_1Routine(spawnPos));

        //_combat.
    }
    IEnumerator Special_1Routine(Vector3 spawnPos)
    {
        Vector3 direction = _fighter.Inverted ? -transform.right : transform.right;
        Vector3 jumpPos = transform.position + transform.up * 0.25f + direction * 0.5f;
        jumpPos = _fighter.CorrectPos(jumpPos,transform.position);
        transform.DOMove(jumpPos, 0.5f);
        _fighter.Animation.SetBool("leg_attack_air_bool",true);
        _fighter.Animation.SetState("Leg_Attack_Air");
        _fighter.Combat.Air_leg_coldier.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.4f);
        // catch fighter
        Vector3 origin = transform.position-Vector3.up*0.5f;
        float distance = 0.7f;
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Debug.DrawRay(origin, direction.normalized * distance, Color.red, 3f);
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, mask);
        Fighter f = null;
        _fighter.transform.DOMoveY(_fighter.LandingPosY, 0.1f);
        if (hit.collider != null)
        {
            f = hit.collider.gameObject.GetComponent<Fighter>();    
        }
        else 
        {
         
            yield return new WaitForSeconds(0.1f);
            _fighter.OnLanding.Invoke(false);
            ResetSpecials();
            yield break;
        }
    
        //transform.DOMove(endPos, 1f);
        if (hit.collider != null)
        {

            if (f.TakeDamage(_special_1_Damage/2f, AttackType.Special)==0)
            {
                yield return new WaitForSeconds(0.1f);
                _fighter.OnLanding.Invoke(false);
                ResetSpecials();
                yield break;
            }
            if (!f.IsOnGround) 
            {
                f.TakeDamage(_special_1_Damage, AttackType.Special);
                yield return new WaitForSeconds(0.1f);
                _fighter.OnLanding.Invoke(false);
                ResetSpecials();
            }
            f.Inputs.ToggleControls(false);
            f.ResetAllActiveStatus();
            yield return new WaitForSeconds(0.1f);
            _fighter.Animation.SetState("special_strike_1_anim");
            _fighter.UpdateColiderEnable(false);
            _fighter.transform.position = new Vector3(f.transform.position.x-direction.x*0.5f, -0.1f, _fighter.transform.position.z);
            f.Animation.SetState("garaev_special_1_reaction");
            Vector3 startFighter = _fighter.CorrectPos(transform.position + direction.normalized * distance, transform.position);
            _fighter.transform.DOMove(startFighter, 2f);
            Vector3 startOpponent= f.CorrectPos(f.transform.position +direction.normalized * distance, f.transform.position);
            f.transform.DOMove(startOpponent, 2f);
            yield return new WaitForSeconds(1.8f);
            f.transform.DOKill();
            _fighter.UpdateColiderEnable(true);
            _fighter.Animation.SetState("garaev_special_1_end");
            yield return new WaitForSeconds(0.2f);
            _fighter.transform.position = new Vector3(_fighter.transform.position.x, _fighter.LandingPosY, _fighter.transform.position.z);
            _fighter.OnLanding.Invoke(false);
            f.TakeDamage(_special_1_Damage / 2f, AttackType.Special);
            ResetSpecials();
            f.Inputs.ToggleControls(true);

        }

       
    }
    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif

        if (!_canDoSpecial)
        {
            return;
        }

        base.Special_2(spawnPos);
        StartCoroutine(Special_2Routine(spawnPos));
    }


    IEnumerator Special_2Routine(Vector3 spawnPos)
    {


        Vector3 origin = spawnPos;
        Vector3 direction = _fighter.Inverted ? -transform.right : transform.right;
        float distance = 0.9f;
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Debug.DrawRay(origin, direction.normalized * distance, Color.red, 1f);
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, mask);

        if (hit.collider != null)
        {
            Fighter f = hit.collider.gameObject.GetComponent<Fighter>();

            if (f.TakeDamage(_special_2_Damage, AttackType.Special) == 0)
            {
                yield break;
            }
            f.Inputs.ToggleControls(false);
            f.ResetAllActiveStatus();
            yield return new WaitForSeconds(0.1f);
            f.Animation.SetState("reaction_uppercut_anim");
            yield return new WaitForSeconds(0.55f);
            f.Animation.SetState("get_up_anim");
            f.Inputs.ToggleControls(true);

        }

    }

    #endregion /Specials
    #region Fatality
    [Space(10)]
    [Header("Fatality")]
    [SerializeField] SpriteRenderer rocket = null;
    [SerializeField] Sprite[] rocketSprites = null;
    [SerializeField] GameObject bloodFXPrefab = null;
    [SerializeField] GameObject _bloodLargePrefab = null;
    public override void Fatality()
    {
        base.Fatality();
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }

    IEnumerator FatalityRoutine()
    {
        OnFatalityStarted.Invoke();
        _fighter.Inputs.ToggleControls(false);
        // move to position
        Vector3 pos = _fighter.Opponent.position - (_fighter.Opponent.position - _fighter.transform.position).normalized * 2.25f;
        float moveDelay = Mathf.Abs(transform.position.x - pos.x) / 1.1f;
        _fighter.MoveToPosition(pos, moveDelay);
     
        yield return new WaitForSeconds(moveDelay);
        // code here
        Camera.main.GetComponent<CameraMover>().MoveToPoseX(Camera.main.transform.position.x + (_fighter.Inverted ? -0.4f : 0.4f),0.5f);

        _fighter.Animation.SetState("fatality_pose");
       
        //set rocket
        float ScaleX = rocket.transform.localScale.x;
        rocket.transform.localScale = new Vector3(_fighter.Inverted ? -ScaleX : ScaleX, rocket.transform.localScale.y, rocket.transform.localScale.z);
        float PosX = rocket.transform.localPosition.x;
        rocket.transform.localPosition = new Vector3(_fighter.Inverted ? -PosX : PosX,rocket.transform.localPosition.y,rocket.transform.localPosition.z);

        yield return new WaitForSeconds(0.75f);
        rocket.gameObject.SetActive(true);
        Vector3 rocketEndPos = _fighter.Opponent.transform.position + new Vector3(!_fighter.Inverted?-1.8f:1.8f, 0.5f, 0);
        rocket.transform.DOMove(rocketEndPos, 0.2f);
        yield return new WaitForSeconds(0.2f);
        rocket.sprite = rocketSprites[1];

        //_fighter.Opponent.GetComponent<Fighter>().Animation.SetState("lay_down");
        _fighter.Opponent.GetComponent<Fighter>().Animation.SetState("FinalDeath");
        Instantiate(bloodFXPrefab, _fighter.Opponent.position+ new Vector3(0,-0.75f,0), Quaternion.identity);
        Instantiate(_bloodLargePrefab, _fighter.Opponent.position + new Vector3(0, -1.35f, 0), Quaternion.identity);

        yield return new WaitForSeconds(0.8f);

        _fighter.Opponent.GetComponent<Animator>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        OnFatalityFinished.Invoke();

    }

    #endregion /Fatality
}