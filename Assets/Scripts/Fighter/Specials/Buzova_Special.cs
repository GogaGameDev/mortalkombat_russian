using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class Buzova_Special : SpecialBase
{
    [SerializeField] float _special_1_Damage = 10f;
    [SerializeField] float _special_2_Damage = 10f;
    [SerializeField] GameObject _screamObj = null;
    [SerializeField] Transform[] opponentMoveStates;
    [SerializeField] Fighter opponent = null;
    Coroutine special1 = null;
    Coroutine special2 = null;
    GameObject projectileObj = null;


    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);

#if UNITY_EDITOR
        if (_fighter._playerID == 1)
        {
            // Fatality();
        }
#endif

    }

    #region Specials
    public override void ResetSpecials()
    {
        Debug.Log(_fighter.FighterName + " ResetSpecials");

       

        if (special1 != null)
        {
            StopCoroutine(special1);
        }
        if (special2 != null)
        {   
            StopCoroutine(special2);
            special2Done = false;
           
            if (opponent != null)
            {
                opponent.UpdateColiderTrigger(false);
                opponent.UpdateColiderEnable(true);
                opponent.Animation.SetTrigger("getup_trigger");
                opponent = null;
            }
        }
    }
    public override void Special_1(Vector3 spawnPos)
    {

#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif


        if (!CanDoSpecial)
        {
            return;
        }
        base.Special_1(spawnPos);
        special1 = StartCoroutine(Special_1Routine(spawnPos));

        //_combat.
    }
    IEnumerator Special_1Routine(Vector3 spawnPos)
    {
        Vector3 origin = spawnPos;
        Vector3 direction = _fighter.Inverted ? -transform.right : transform.right;
        float distance = 0.7f;
        GameObject screamObj = Instantiate(_screamObj, origin + direction.normalized * distance * 0.8f, Quaternion.identity);
        screamObj.GetComponent<SpriteRenderer>().flipX = _fighter.Inverted;
        Destroy(screamObj, 1.5f);
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Debug.DrawRay(origin, direction.normalized * distance, Color.red, 3f);
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, mask);

        if (hit.collider != null)
        {
            Fighter f = hit.collider.gameObject.GetComponent<Fighter>();

            if (f.TakeDamage(_special_1_Damage, AttackType.Special) == 0)
            {
                yield break;
            }

            yield return new WaitForSeconds(0.1f);
            f.ResetAllActiveStatus();
            yield return new WaitForSeconds(0.1f);
            f.Inputs.ToggleControls(false);
            f.Animation.SetTrigger("buzova_dance_trigger");

            //dance dance revolution
            yield return new WaitForSeconds(1.4f);
            if (f.Health == 0)
            {
                f.OnStunn.Invoke();
                yield break;
            }

            f.Inputs.ToggleControls(true);

        }

    }
    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif


        if (!CanDoSpecial)
        {
            return;
        }
        base.Special_2(spawnPos);
        special2 = StartCoroutine(Special_2Routine(spawnPos));
    }

    [SerializeField] bool special2Done = false;
    IEnumerator Special_2Routine(Vector3 spawnPos)
    {

        special2Done = false;
        _fighter.IsDoingSpecial = true;
        Vector3 origin = spawnPos;
        Vector3 direction = _fighter.Inverted ? -transform.right : transform.right;
        float distance = 1.5f;
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Debug.DrawRay(origin, direction.normalized * distance, Color.red, 1f);
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, mask);

        if (hit.collider != null)
        {

            Fighter f = hit.collider.gameObject.GetComponent<Fighter>();

            if (f.TakeDamage(_special_2_Damage, AttackType.Special) == 0)
            {
                yield break;
            }
            f.Inputs.ToggleControls(false);
            f.ResetAllActiveStatus();
            opponent = f;
            opponent.IsUnderSpecial = true;
            yield return new WaitForSeconds(0.1f);
            inverted = _fighter.Inverted;
            f.Animation.SetTrigger("buzova_hair_trigger");
            Invoke("ResetSpecials", 2f);
            yield return new WaitUntil(() => special2Done);

            f.transform.position = new Vector3(f.transform.position.x, 0.159f, f.transform.position.z);
            f.transform.DOShakePosition(0.5f, Vector3.up * 0.1f, 10, 0);
            yield return new WaitForSeconds(0.55f);

            if (f.Health > 0)
            {
                f.Animation.SetTrigger("getup_trigger");
                yield return new WaitForSeconds(0.65f);
                f.Inputs.ToggleControls(true);
            }
            else
            {
                if (GameManager.IsEndGame())
                {
                    f.Animation.SetTrigger("getup_trigger");
                    yield return new WaitForSeconds(0.65f);
                    f.Stunn(0, false, true);
                }
                else
                {
                    f.TakeDamage(_special_2_Damage, AttackType.Special);
                }

            }

            opponent.IsUnderSpecial = false;
            _fighter.IsDoingSpecial = false;
            opponent = null;
        }

    }


    bool inverted;
    public void MoveByHair(int state)
    {


        if (opponent == null)
        {
            return;
        }
        opponentMoveStates[state].localPosition = new Vector3(inverted ? -Mathf.Abs(opponentMoveStates[state].localPosition.x) : Mathf.Abs(opponentMoveStates[state].localPosition.x), opponentMoveStates[state].localPosition.y, opponentMoveStates[state].localPosition.z);

        if (state != 0)
        {
            opponentMoveStates[state].localPosition = new Vector3(inverted ? Mathf.Abs(opponentMoveStates[state].localPosition.x) : -Mathf.Abs(opponentMoveStates[state].localPosition.x), opponentMoveStates[state].localPosition.y, opponentMoveStates[state].localPosition.z);

        }
        opponent.UpdateSide();
        opponent.transform.position = opponent.CorrectPos(opponentMoveStates[state].position, opponent.transform.position);

        if (state == 2)
        {
            special2Done = true;
        }
    }


    #endregion /Specials
    #region Fatality
    [Space(10)]
    [Header("Fatality")]
    [SerializeField] FighterUISelection[] _otherFighters;
    [SerializeField] Transform[] _otherFightersPos;
    [SerializeField] GameObject _fireObj = null;
    [SerializeField] GameObject _skeleton = null;
    public override void Fatality()
    {
        base.Fatality();
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }

    IEnumerator FatalityRoutine()
    {
        OnFatalityStarted.Invoke();
     
        // move to position
        Vector3 pos = _fighter.Opponent.position - (_fighter.Opponent.position - _fighter.transform.position).normalized * 0.75f;
        float moveDelay = Mathf.Abs(transform.position.x - pos.x) / 1.1f;
        _fighter.MoveToPosition(pos, moveDelay);
        yield return new WaitForSeconds(moveDelay);

        // place bonifire
        _fighter.Animation.SetState("Use");
        Vector3 fireSpawnPos = _fighter.Opponent.transform.position + new Vector3(0, -1.25f, 0);
        Animator fireAnim = Instantiate(_fireObj, fireSpawnPos, Quaternion.identity).GetComponent<Animator>();
        yield return new WaitForSeconds(0.5f);
        _fighter.Animation.SetState("IDL");
        yield return new WaitForSeconds(0.5f);
        _fighter.Animation.SetState("Use");
        fireAnim.enabled = true;
        yield return new WaitForSeconds(0.2f);
        _fighter.Animation.SetState("IDL");

        // spawnSkeleton
        Vector3 skeletonSPawnPos = _fighter.Opponent.transform.position + new Vector3(0, -0.15f, 0);
        GameObject skeletonObj = Instantiate(_skeleton, skeletonSPawnPos, Quaternion.identity);
        skeletonObj.transform.localScale = new Vector3(_fighter.Inverted ? -1f : 1f, 1f, 1f);
        _fighter.Opponent.gameObject.SetActive(false);

        //moveTo sit Position 
        Vector3 endPos = _fighter.Opponent.transform.position + new Vector3(0, 0.35f, 0);
        moveDelay = 1f;
        _fighter.MoveToPosition(endPos, moveDelay);
        yield return new WaitForSeconds(moveDelay);
        _fighter.RB.constraints = RigidbodyConstraints2D.FreezeAll;
        _fighter.Animation.SetState("Sit");

        for (int i = 0; i < _otherFighters.Length; i++)
        {
            if (_otherFighters[i].FighterID == _fighter.Opponent.GetComponent<Fighter>().FighterName)
            {
                continue;
            }
            Vector3 direction = _otherFightersPos[i].localPosition.x > 0 ? _fighter.transform.right : -_fighter.transform.right;
            Vector3 spawnPos = _otherFightersPos[i].transform.position + direction.normalized * 3f;
            _otherFighters[i].transform.position = spawnPos;
            _otherFighters[i].transform.localScale = new Vector3(_otherFightersPos[i].localPosition.x > 0 ? -_otherFighters[i].transform.localScale.x : _otherFighters[i].transform.localScale.x, _otherFighters[i].transform.localScale.y, _otherFighters[i].transform.localScale.z);
            _otherFighters[i].gameObject.SetActive(true);
            _otherFighters[i].gameObject.name = _otherFightersPos[i].gameObject.name;
            MoveToPosAndSit(_otherFighters[i], _otherFightersPos[i].position + new Vector3(0, 0, spawnPos.y));
        }

        yield return new WaitForSeconds(2f);
        _fighter.gameObject.GetComponent<Animator>().enabled = false;
        OnFatalityFinished.Invoke();

    }
    void MoveToPosAndSit(FighterUISelection f, Vector3 pos)
    {
        f.SetAnimation("IDL");
        f.SetAnimation("walking_bool", true);

        f.transform.DOMove(pos, 1f).OnComplete(() =>
        {
            f.SetState("Sit");
        });

    }
    #endregion /Fatality
}