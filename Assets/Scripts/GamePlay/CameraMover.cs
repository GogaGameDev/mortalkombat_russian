using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraMover : MonoBehaviour
{
    [SerializeField] Transform[] _players;
    [SerializeField] float _speed = 3f;
    [SerializeField] Vector2 bordersX;
    Camera _camera = null;
    bool canMove = true;
    public void Init(Fighter[] fighters)
    {
        _players = new Transform[2];
        _players[0] = fighters[0].transform;
        _players[1] = fighters[1].transform;
    }

    void Start()
    {
        _camera = this.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void LateUpdate()
    {
        if (canMove)
        {
            float leftPos = Mathf.Min(_players[0].transform.position.x, _players[1].transform.position.x);
            float maxY = Mathf.Max(_players[0].transform.position.y, _players[1].transform.position.y);
            maxY = Mathf.Clamp(maxY, 0.14f, 0.25f);
            float newPosx = leftPos + Mathf.Abs(_players[0].transform.position.x - _players[1].transform.position.x) / 2f;
            if (!GameManager.gameOver)
            {
                newPosx = Mathf.Clamp(newPosx, bordersX.x, bordersX.y);
            }
            Vector3 newPos = new Vector3(newPosx, transform.position.y, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, newPos, _speed * Time.deltaTime);
        }
    }
    // Fatality Only
    public void MoveToPoseX( float posX, float time)
    {
        canMove = false;
        transform.DOMoveX(posX, time);
       
    }
   
}