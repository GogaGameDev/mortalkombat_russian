﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Audio.SoundPlayers
{
    /// <summary>
    /// Base class for sound and music playback
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class SoundPlayer : MonoBehaviour
    {
        public virtual void Play()
        {
            _audioSource.Play();
        }

        private protected void Play(AudioClip audioClip)
        {
            _audioSource.clip = audioClip;
            _audioSource.Play();
        }

        private protected void PlayOnce(AudioClip audioClip)
        {
            _audioSource.PlayOneShot(audioClip);
        }

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        [Button(ButtonSizes.Gigantic)]
        protected void TestPlayback()
        {
            _audioSource = GetComponent<AudioSource>();
            Play();
        }

        private AudioSource _audioSource;
    }
}