using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class Projectile : MonoBehaviour
{
    [SerializeField] SpriteRenderer _spriteRenderer;
    GameObject _user;
    [HideInInspector] public EventGameObject _OnHit = new EventGameObject();
    Animator _anim;
    [SerializeField] LayerMask mask;
    [SerializeField] string layerStringValue = "";
    private void Start()
    {
        _anim = this.GetComponent<Animator>();
    }
    public void InitThrow(bool inverted, Fighter user)
    {
        this._user = user.gameObject;
        layerStringValue = user._playerID == 1 ? "p2" : "p1";
        mask = LayerMask.NameToLayer(layerStringValue);
        _spriteRenderer.flipX = inverted;
        float endXpos = !_spriteRenderer.flipX ? transform.position.x + 10f : transform.position.x - 10f;
        transform.DOMoveX(endXpos, 2f).OnComplete(() =>
        {
            Destroy(this.gameObject);
        });
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer(layerStringValue))
        {
            _OnHit.Invoke(collision.gameObject);
            transform.DOKill();
            //  Destroy(this.gameObject,2f);
            _anim.SetTrigger("hit_trigger");
        }
        // 
    }

}