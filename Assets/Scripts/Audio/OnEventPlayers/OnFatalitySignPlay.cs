﻿using DG.Tweening;
using UnityEngine;

namespace Audio
{
    public class OnFatalitySignPlay : OnEventPlay
    {
        [SerializeField] private UIManager _uiManager;
        [SerializeField] private float _delay = 1f;

        protected override void SubscribeToEvent()
        {
            _uiManager.OnFatalityShowed.AddListener(DelayedPlay);
        }

        private void DelayedPlay()
        {
            DOTween.Sequence().AppendInterval(_delay).AppendCallback(PlaySound);
        }
    }
}