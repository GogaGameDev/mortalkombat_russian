using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using DG.Tweening;
public class UIManager : MonoBehaviour
{

    [Header("Core")]
    [SerializeField] TextMeshProUGUI _winner_Text = null;
    public GameObject _finishHimObj = null;
    public GameObject _finishHerObj = null;
    [SerializeField] GameObject _fatalityObj = null;
    [SerializeField] GameObject _fightAnimObj = null;
    [SerializeField] Image _blackScreenoverlay = null;
    [Space(10)]
    [Header("Player")]
    [SerializeField] private Image[] _pHealthBarFill_Image = null;
    [SerializeField] private Transform[] pCoinsContainer;
    [SerializeField] private GameObject coinPrefab = null;
    [SerializeField] private TextMeshProUGUI[] _pCharacterName_Text = null;
    [Space(10)]
    [Header("Timer")]
    public Timer _timer = null;
    public UnityEvent OnPrematchAnimationsEnded = new UnityEvent();
    public UnityEvent OnFatalityShowed = new UnityEvent();


    void Start()
    {

        ShowStartAnims();

    }

    public void ShowStartAnims()
    {
        _winner_Text.gameObject.SetActive(false);
        StartCoroutine(ShowPreMatchAnimationsRoutine(2f));
    }
    IEnumerator ShowPreMatchAnimationsRoutine(float delay)
    {

        FadeOut(3f);
        yield return new WaitForSeconds(1f);
        _fightAnimObj.gameObject.SetActive(true);
        _timer.StartTimer();
        yield return new WaitForSeconds(delay);

        OnPrematchAnimationsEnded.Invoke();

    }
    void UpdateHealthBar(int playerID, float healthStatus)
    {
        _pHealthBarFill_Image[playerID - 1].fillAmount = healthStatus / 100f;
    }

    public void SetWinner()
    {
        if (_pHealthBarFill_Image[0].fillAmount == _pHealthBarFill_Image[1].fillAmount)
        {
            _winner_Text.text = " FRIENDSHIP ";

            Instantiate(coinPrefab, pCoinsContainer[0]);
            Instantiate(coinPrefab, pCoinsContainer[1]);
        }
        else
        {
            string winnerName = _pHealthBarFill_Image[0].fillAmount > _pHealthBarFill_Image[1].fillAmount ? _pCharacterName_Text[0].text : _pCharacterName_Text[1].text;
            Transform container = _pHealthBarFill_Image[0].fillAmount > _pHealthBarFill_Image[1].fillAmount ? pCoinsContainer[0] : pCoinsContainer[1];
            _winner_Text.text = winnerName + " WINS ";
            Instantiate(coinPrefab, container);

        }

        _winner_Text.gameObject.SetActive(true);
    }
    public void RegisterFighter(Fighter f)
    {

        _pCharacterName_Text[f._playerID - 1].text = f.FighterName;
        f.OnHealthChange.AddListener(UpdateHealthBar);
    }
    public void ShowFatality(float delay)
    {
        StartCoroutine(ShowFatalityRoutine(delay));
    }

    IEnumerator ShowFatalityRoutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        _fatalityObj.gameObject.SetActive(true);
        OnFatalityShowed.Invoke();
    }
    public void FadeIn(float duration)
    {
        _blackScreenoverlay.DOFade(1f, duration);
    }
    public void FadeOut(float duration)
    {
        Color col = Color.black;
        col.a = 1f;
        _blackScreenoverlay.color = col;
        _blackScreenoverlay.DOFade(0f, duration);
    }
}