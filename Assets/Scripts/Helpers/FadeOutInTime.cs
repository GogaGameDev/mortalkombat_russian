using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FadeOutInTime : MonoBehaviour
{

    [SerializeField] Image _image;


    public void FadeOut(float time) 
    {
        Color newColor = _image.color;
        newColor.a = 0;
        _image.DOColor(newColor, time);
    }
}
