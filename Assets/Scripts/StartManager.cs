using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using System.Linq;
using UnityEngine.SceneManagement;

public class StartManager : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI[] playerText;
    List<int> deviceIDList = new List<int>();
    Coroutine loadRoutine = null;

    void Start()
    {
        Screen.SetResolution(960, 720, true);
        InputSystem.onDeviceChange += DeviceCHange;

        CheckPlayers();
    }

    // Update is called once per frame

    void CheckPlayers()
    {

        if (Gamepad.all.Count > 0)
        {
            if (!deviceIDList.Contains(Gamepad.all[0].deviceId))
            {
                deviceIDList.Add(Gamepad.all[0].deviceId);
            }
            playerText[0].text = "Player 1\nIs Ready...";
        }
        else
        {
            playerText[0].text = "Player 1\nConnect Gamepad...";
        }
        if (Gamepad.all.Count > 1)
        {
            if (!deviceIDList.Contains(Gamepad.all[1].deviceId))
            {
                deviceIDList.Add(Gamepad.all[1].deviceId);
            }

            playerText[1].text = "Player 2\nIs Ready...";
        }
        else
        {
            playerText[1].text = "Player 2\nConnect Gamepad...";
        }

        if (deviceIDList.Count == 2)
        {
            loadRoutine = StartCoroutine(LoadScene(1f));
        }
    }
    IEnumerator LoadScene(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("CharacterSelection");
    }
    public void DeviceCHange(InputDevice d, InputDeviceChange c)
    {
        Debug.Log(d.displayName + " connected..." + c);


        CheckPlayers();

    }
}