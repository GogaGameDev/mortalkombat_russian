using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Chorovod : MonoBehaviour
{
    public UnityEvent OnHideFighter;


    public void HideFighter() 
    {
        OnHideFighter.Invoke();
    }
}
