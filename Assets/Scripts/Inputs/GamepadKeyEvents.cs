﻿using System;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;

namespace Inputs
{
    public class GamepadKeyEvents : MonoBehaviour
    {
        public static event Action<int> WestPressed;
        public static event Action<int> SouthPressed;
        public static event Action<int> NorthPressed;
        public static event Action<int> EastPressed;

        public static event Action<int> DpadUpPressed;
        public static event Action<int> DpadDownPressed;
        public static event Action<int> DpadRightPressed;
        public static event Action<int> DpadLeftPressed;

        public static event Action<int> LeftShoulderPressed;
        public static event Action<int> RightShoulderPressed;

        public static event Action<int> LeftTriggerPressed;
        public static event Action<int> RightTriggerPressed;

        public static void InvokeGamepadKeyEvent(GamepadButton gamepadButton, int id)
        {
            switch (gamepadButton)
            {
                case GamepadButton.DpadUp:
                    DpadUpPressed?.Invoke(id);
                    break;
                case GamepadButton.DpadDown:
                    DpadDownPressed?.Invoke(id);
                    break;
                case GamepadButton.DpadLeft:
                    DpadLeftPressed?.Invoke(id);
                    break;
                case GamepadButton.DpadRight:
                    DpadRightPressed?.Invoke(id);
                    break;
                case GamepadButton.North:
                    NorthPressed?.Invoke(id);
                    break;
                case GamepadButton.East:
                    EastPressed?.Invoke(id);
                    break;
                case GamepadButton.South:
                    SouthPressed?.Invoke(id);
                    break;
                case GamepadButton.West:
                    WestPressed?.Invoke(id);
                    break;
                case GamepadButton.LeftShoulder:
                    LeftShoulderPressed?.Invoke(id);
                    break;
                case GamepadButton.RightShoulder:
                    RightShoulderPressed?.Invoke(id);
                    break;
                case GamepadButton.LeftTrigger:
                    LeftTriggerPressed?.Invoke(id);
                    break;
                case GamepadButton.RightTrigger:
                    RightTriggerPressed?.Invoke(id);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(gamepadButton), gamepadButton, null);
            }
        }
    }
}