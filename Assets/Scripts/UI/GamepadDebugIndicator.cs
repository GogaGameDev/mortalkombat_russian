﻿using DG.Tweening;
using Inputs;
using UnityEngine;

namespace UI
{
    public class GamepadDebugIndicator : MonoBehaviour
    {
        [SerializeField] private float _pressDuration = 0.2f;
        [SerializeField] private int _playerId = 1;
        [SerializeField] private GameObject _arrowLeft;
        [SerializeField] private GameObject _arrowRight;
        [SerializeField] private GameObject _arrowUp;
        [SerializeField] private GameObject _arrowDown;

        [SerializeField] private GameObject _actionA;
        [SerializeField] private GameObject _actionB;
        [SerializeField] private GameObject _actionX;
        [SerializeField] private GameObject _actionY;

        [SerializeField] private GameObject _shoulderLeft;
        [SerializeField] private GameObject _shoulderRight;

        [SerializeField] private GameObject _triggerLeft;
        [SerializeField] private GameObject _triggerRight;


        private void OnEnable()
        {
            GamepadKeyEvents.SouthPressed += OnSouthPressed;
            GamepadKeyEvents.EastPressed += OnEastPressed;
            GamepadKeyEvents.WestPressed += OnWestPressed;
            GamepadKeyEvents.NorthPressed += OnNorthPressed;

            GamepadKeyEvents.DpadDownPressed += OnDpadDownPressed;
            GamepadKeyEvents.DpadUpPressed += OnDpadUpPressed;
            GamepadKeyEvents.DpadLeftPressed += OnDpadLeftPressed;
            GamepadKeyEvents.DpadRightPressed += OnDpadRightPressed;

            GamepadKeyEvents.LeftTriggerPressed += OnLeftTriggerPressed;
            GamepadKeyEvents.RightTriggerPressed += OnRightTriggerPressed;

            GamepadKeyEvents.LeftShoulderPressed += OnLeftShoulderPressed;
            GamepadKeyEvents.RightShoulderPressed += OnRightShoulderPressed;
        }

        private void OnDisable()
        {
            GamepadKeyEvents.SouthPressed -= OnSouthPressed;
            GamepadKeyEvents.EastPressed -= OnEastPressed;
            GamepadKeyEvents.WestPressed -= OnWestPressed;
            GamepadKeyEvents.NorthPressed -= OnNorthPressed;

            GamepadKeyEvents.DpadDownPressed -= OnDpadDownPressed;
            GamepadKeyEvents.DpadUpPressed -= OnDpadUpPressed;
            GamepadKeyEvents.DpadLeftPressed -= OnDpadLeftPressed;
            GamepadKeyEvents.DpadRightPressed -= OnDpadRightPressed;

            GamepadKeyEvents.LeftTriggerPressed -= OnLeftTriggerPressed;
            GamepadKeyEvents.RightTriggerPressed -= OnRightTriggerPressed;

            GamepadKeyEvents.LeftShoulderPressed -= OnLeftShoulderPressed;
            GamepadKeyEvents.RightShoulderPressed -= OnRightShoulderPressed;
        }

        private void OnRightShoulderPressed(int i)
        {
            _currentPressedPlayerId = i;
            ShowKeyPressed(_shoulderRight);
        }

        private void OnLeftShoulderPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_shoulderLeft);
        }

        private void OnRightTriggerPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_triggerRight);
        }

        private void OnLeftTriggerPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_triggerLeft);
        }

        private void OnDpadRightPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_arrowRight);
        }

        private void OnDpadLeftPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_arrowLeft);
        }

        private void OnDpadUpPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_arrowUp);
        }

        private void OnDpadDownPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_arrowDown);
        }

        private void OnNorthPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_actionY);
        }

        private void OnWestPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_actionX);
        }

        private void OnEastPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_actionB);
        }

        private void OnSouthPressed(int i)
        {
            _currentPressedPlayerId = i;

            ShowKeyPressed(_actionA);
        }

        private void ShowKeyPressed(GameObject gameObject)
        {
            if (_currentPressedPlayerId != _playerId)
                return;

            DOTween.Kill(gameObject);
            DOTween.Sequence().AppendCallback(() => gameObject.SetActive(true))
                .AppendInterval(_pressDuration)
                .AppendCallback(() => gameObject.SetActive(false)).SetId(gameObject);
        }

        private int _currentPressedPlayerId;
    }
}