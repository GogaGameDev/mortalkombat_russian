using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class GameManager : MonoBehaviour
{
    [Header("Arena")]
    [SerializeField] Arena _arena = null;
    [SerializeField] Color _arenaFatalityColor = Color.white;
    [SerializeField] ParticleSystem[] _bloodParticle = null;
    [Header("UI")]
    [SerializeField] UIManager _uiManager = null;
    [SerializeField] CameraMover _camera = null;
    public Fighter[] _playerFighters = new Fighter[2];
    [SerializeField] GameObject shadowPrefab;
    private static int[] winCount;
    private static int maxRoundCount;
    [SerializeField] MatchParams _matchProps = null;

    public UnityEvent OnGameOver;
    public UnityEvent OnMatchStart;
    public UnityEvent OnRestart;
    public UnityEvent OnEndgameStarted;
    public Gender FinishGender;
    public Fighter WinnerFighter;
    public static bool gameOver;
    public static bool fatalityAvaliable;
    public static int roundID;

    private void Awake()
    {

        winCount = new int[2];
        maxRoundCount = _matchProps.roundCount;
        roundID = 0;
        fatalityAvaliable = false;
        // arena
        _arena = Instantiate(_matchProps.arenaPrefab, new Vector3(-0.027f, 0.29f, 2f), Quaternion.identity).GetComponent<Arena>();

        foreach (var item in _playerFighters)
        {
            if (item != null)
            {
                Destroy(item.gameObject);
            }
        }


        _playerFighters[0] = Instantiate<Fighter>(_matchProps.player_FighterPrefab[0], new Vector3(-1.25f, 0.159f, 0), Quaternion.identity);
        _playerFighters[0].gameObject.name += "_P1";
        _playerFighters[1] = Instantiate<Fighter>(_matchProps.player_FighterPrefab[1], new Vector3(1.25f, 0.159f, 0), Quaternion.identity);
        _playerFighters[1].gameObject.name += "_P2";
        _playerFighters[0].Init(1, _playerFighters[1].transform);
        _playerFighters[0].ZeroHealh.AddListener(() =>
        {
            winCount[1]++;
            if (!_playerFighters[1].IsDoingSpecial && IsEndGame())
            {
                _playerFighters[1].ResetAllActiveStatus(_playerFighters[0].IsKnockedDown ? 1f : 0f);
            }
        });
        _playerFighters[1].Init(2, _playerFighters[0].transform);
        _playerFighters[1].ZeroHealh.AddListener(() =>
        {
            winCount[0]++;
            if (!_playerFighters[0].IsDoingSpecial && IsEndGame())
            {
                _playerFighters[0].ResetAllActiveStatus(_playerFighters[1].IsKnockedDown ? 1f : 0f);
            }

        });

        foreach (var p in _playerFighters)
        {
            FighterShadow shadow = Instantiate(shadowPrefab, p.transform.position, Quaternion.identity).GetComponent<FighterShadow>();
            shadow.Init(p);
            p.OnDeath.AddListener(GameOver);

            p.OnFinalDeath.AddListener(() =>
            {
                StartCoroutine(GameOverOnDelayRoutine(1));
            });

            p.OnTakingDamage.AddListener((playerID) =>
            {
                EmitBlood(playerID);
                UpdateLayers(playerID);
            });
            _uiManager.RegisterFighter(p);

            FighterInputs inputs = p.GetComponent<FighterInputs>();
            inputs.Init(p);

            FighterCombat fCombat = p.GetComponent<FighterCombat>();
            fCombat.OnAttack.AddListener(DamagePlayer);

            fCombat.Special.OnFatalityStarted.AddListener(() =>
            {
                StartCoroutine(ShadowBackGroundStageRoutine(0));
            });
            fCombat.Special.OnFatalityFinished.AddListener(() =>
            {
                StartCoroutine(GameOverOnDelayRoutine(1, 1f, true));
            });



        }
        //UI
        _uiManager._timer.SetTime(_matchProps.timer);
        OnGameOver.AddListener(_uiManager.SetWinner);
        _uiManager.OnPrematchAnimationsEnded.AddListener(StartMatch);
        OnRestart.AddListener(_uiManager.ShowStartAnims);
        //Camera
        _camera.Init(_playerFighters);
        gameOver = false;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Alpha1)) 
        {
            _playerFighters[1].TakeDamage(100, AttackType.Hand_Mid);

        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _playerFighters[0].TakeDamage(100, AttackType.Hand_Mid);

        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _playerFighters[1].ChangeHealth(10f);

        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _playerFighters[0].Combat.Special.Fatality();

        }
        if (Input.GetKeyDown(KeyCode.Alpha5)) 
        {
            _playerFighters[0].Combat.SpecialAttack(1);
            _playerFighters[1].Combat.SpecialAttack(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            _playerFighters[0].Inputs.OnSpecalInput.Invoke(2);
            _playerFighters[1].Inputs.OnSpecalInput.Invoke(1);
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            _playerFighters[1].OnAttack.Invoke(AttackType.Hand_Low_UpperCut);//.OnSpecalInput.Invoke(2);
          
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            _playerFighters[0].OnAttack.Invoke(AttackType.Hand_Mid);
            _playerFighters[1].OnAttack.Invoke(AttackType.Hand_Low_UpperCut);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            _playerFighters[0].OnAttack.Invoke(AttackType.Leg_Mid);
            _playerFighters[1].OnAttack.Invoke(AttackType.Leg_Mid);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            _playerFighters[0].ResetAllActiveStatus();

        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            _playerFighters[0].Inputs.OnSpecalInput.Invoke(1);

        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            _playerFighters[0].Inputs.OnSpecalInput.Invoke(2);

        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (Time.timeScale != 1)
            {
                Time.timeScale = 1;
            }
            else 
            {
                Time.timeScale = 0.1f;
            }
        }
#endif
    }


    IEnumerator Reset()
    {
        _uiManager.FadeIn(2f);
        yield return new WaitForSeconds(2f);
        _uiManager._timer.SetTime(_matchProps.timer);
        _playerFighters[0].Reset();
        _playerFighters[1].Reset();
        gameOver = false;
        _isGameOver = false;
        OnRestart.Invoke();

    }


    IEnumerator ShadowBackGroundStageRoutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        _arena.FadeBackround();
    }
    void GameOver(int deadPlayerID)
    {
        if (gameOver)
            return;

        gameOver = true;
        roundID += 1;
        _uiManager._timer.StopTimer();

        if (deadPlayerID == -1)
        {
            winCount[0]++;
            winCount[1]++;
            return;
        }

        if (IsEndGame())
        {
            if (_playerFighters[deadPlayerID - 1]._gender == Gender.Female)
            {
                _uiManager._finishHerObj.SetActive(true);
            }
            if (_playerFighters[deadPlayerID - 1]._gender == Gender.Male)
            {
                _uiManager._finishHimObj.SetActive(true);
            }

            FinishGender = _playerFighters[deadPlayerID - 1]._gender;
            OnEndgameStarted.Invoke();
        }

        if (IsEndGame())
        {
            fatalityAvaliable = true;
        }
    }
    IEnumerator GameOverOnDelayRoutine(float delay, float restartDelay = 3, bool fatality = false)
    {
        if (_isGameOver)
        {
            _isGameOver = false;
            yield break;
        }

        gameOver = true;
        _isGameOver = true;

        foreach (var item in _playerFighters)
        {
            if (item.IsDoingSpecial)
            {
                yield return new WaitForSeconds(1f);
            }

            item.Inputs.ToggleControls(false);
        }

        yield return new WaitForSeconds(delay);

        OnGameOver.Invoke();

        foreach (var item in _playerFighters)
        {
            if (!item.IsStunned)
            {
                WinnerFighter = item;
                item.Animation.SetTrigger("win_trigger");
            }

        }

        if (fatality)
        {
            _uiManager.ShowFatality(1f);
            yield return new WaitForSeconds(2f);
        }

        yield return new WaitForSeconds(1f);

        if (IsEndGame())
        {

            _uiManager.FadeIn(2f);
            yield return new WaitForSeconds(1f);
            yield return new WaitForSeconds(restartDelay);
            LoadScene("CharacterSelection");
        }
        else
        {

            StartCoroutine(Reset());
        }


    }

    public void LoadScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }
    public void StartMatch()
    {
        OnMatchStart.Invoke();
        foreach (var f in _playerFighters)
        {
            f.Inputs.ToggleControls(true);
            f.GetLandingPos();
        }

    }
    public void TimesUp()
    {
        if (_playerFighters[0].Health == _playerFighters[1].Health)
        {
            GameOver(-1);
            _playerFighters[0].StopMovement();
            _playerFighters[1].StopMovement();
            StartCoroutine(GameOverOnDelayRoutine(1));
            return;
        }
        int loosePlayer = _playerFighters[0].Health > _playerFighters[1].Health ? 2 : 1;
        _playerFighters[loosePlayer - 1].Stunn(0, false);
        _playerFighters[loosePlayer - 1].ChangeHealth(0);
        GameOver(loosePlayer);
    }
    void DamagePlayer(int playerID, float damage, AttackType type)
    {
        _playerFighters[playerID == 1 ? 1 : 0].TakeDamage(damage, type);
        _playerFighters[0].SetSortLayer(playerID == 1 ? 2 : 1);
        _playerFighters[1].SetSortLayer(playerID == 1 ? 1 : 2);
    }

    void EmitBlood(Vector3 position)
    {
        _bloodParticle[1].transform.position = position;
        _bloodParticle[0].Emit(UnityEngine.Random.Range(1, 3));
        _bloodParticle[1].Emit(UnityEngine.Random.Range(2, 4));
    }
    void EmitBlood(int playerID)
    {
        EmitBlood(_playerFighters[playerID == 1 ? 0 : 1].transform.position);
    }

    public static bool IsEndGame()
    {
        if (winCount[0] == maxRoundCount || winCount[1] == maxRoundCount)
        {
            return true;
        }

        return false;
    }
    void UpdateLayers(int damageTakenPlayerID)
    {
        _playerFighters[0].SetSortLayer(damageTakenPlayerID == 1 ? 1 : 2);
        _playerFighters[1].SetSortLayer(damageTakenPlayerID == 1 ? 2 : 1);
    }

    private bool _isGameOver;
}