using System.Collections;
using System.Collections.Generic;
using Inputs;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;

public class FighterInputs : MonoBehaviour
{
    FighterControls _inputs;
    Fighter _fighter = null;
    private bool _moveLeft = false;
    private bool _moveRight = false;
    private bool _isMoving = false;
    private bool _isCrouching = false;
    [SerializeField] private bool _isBlocking = false;
    [SerializeField] private List<int> _blockList = new List<int>();
    float specialTiming = 0.25f;
    SpecialDirection specialDirection;
    float checkTime = 0;
    //Events
    [HideInInspector] public EventMove OnMove = new EventMove();
    [HideInInspector] public EventInt OnAttackHand = new EventInt();
    [HideInInspector] public UnityEvent OnAttackLeg;
    [HideInInspector] public UnityEvent OnAttackEnded;
    [HideInInspector] public UnityEvent OnStopMoving;
    [HideInInspector] public UnityEvent OnJump;
    [HideInInspector] public EventBool OnCrouch = new EventBool();
    [HideInInspector] public EventBool OnBlock = new EventBool();
    [HideInInspector] public EventInt OnSpecalInput = new EventInt();
    [HideInInspector] public UnityEvent OnFatality = new UnityEvent();

    public void Init(Fighter fighter)
    {
        _fighter = fighter;

        _inputs = new FighterControls();



        _fighter.OnDeath.AddListener((playerID) => { _inputs.Disable(); });
        _fighter.OnBlockBreak.AddListener(() =>
        {
            _blockList = new List<int>();
            _isBlocking = false;
        });
        _inputs.Fighter.Move_LeftStart.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.DpadLeft, _fighter._playerID);
            _moveLeft = true;
            checkTime = Time.time;
            specialDirection = _fighter.Inverted ? SpecialDirection.Forward : SpecialDirection.Backward;


        };

        _inputs.Fighter.Move_LeftEnd.performed += ctx =>
        {

            _moveLeft = false;

        };
        _inputs.Fighter.Move_Right_Start.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.DpadRight, _fighter._playerID);
            _moveRight = true;
            checkTime = Time.time;
            specialDirection = _fighter.Inverted ? SpecialDirection.Backward : SpecialDirection.Forward;


        };

        _inputs.Fighter.Move_Right_End.performed += ctx => { _moveRight = false; };
        _inputs.Fighter.Jump.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.DpadUp, _fighter._playerID);
            OnJump.Invoke();
        };
        _inputs.Fighter.Move_Crouch_Start.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.DpadDown, _fighter._playerID);
            _isCrouching = true;
        };
        _inputs.Fighter.Move_Crouch_End.performed += ctx => { _isCrouching = false; };
        _inputs.Fighter.LegAttack.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.South, _fighter._playerID);
            if (GameManager.gameOver && GameManager.fatalityAvaliable)
            {
                OnFatality.Invoke();
                return;
            }

            if (!PerformSpecial(_inputs.Fighter.LegAttack))
            {
                OnAttackLeg.Invoke();
            }


        };
        _inputs.Fighter.LegAttack_2.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.East, _fighter._playerID);
            if (GameManager.gameOver && GameManager.fatalityAvaliable)
            {
                OnFatality.Invoke();
                return;
            }

            if (!PerformSpecial(_inputs.Fighter.LegAttack_2))
            {
                OnAttackLeg.Invoke();
            }


        };
        _inputs.Fighter.HandAttack.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.West, _fighter._playerID);
            if (GameManager.gameOver && GameManager.fatalityAvaliable)
            {
                OnFatality.Invoke();
                return;
            }
            if (!PerformSpecial(_inputs.Fighter.HandAttack))
            {
                OnAttackHand.Invoke(1);
            }


        };
        _inputs.Fighter.HandAttack_2.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.North, _fighter._playerID);
            if (GameManager.gameOver && GameManager.fatalityAvaliable)
            {
                OnFatality.Invoke();
                return;
            }
            if (!PerformSpecial(_inputs.Fighter.HandAttack_2))
            {
                OnAttackHand.Invoke(2);
            }

        };
        //_inputs.Fighter.Analog.performed += ctx => { if (ctx.control.device.deviceId == _deviceID) { _analogMovement = ctx.ReadValue<Vector2>(); } };
        //_inputs.Fighter.Analog.canceled += ctx => { if (ctx.control.device.deviceId == _deviceID) { _analogMovement = Vector2.zero; } };
        //Blocks
        _inputs.Fighter.Block_1.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.LeftShoulder, _fighter._playerID);
            if (!_blockList.Contains(1)) { _blockList.Add(1); }
        };
        _inputs.Fighter.UnBlock_1.performed += ctx => { if (_blockList.Contains(1)) { _blockList.Remove(1); } };
        _inputs.Fighter.Block_2.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.LeftTrigger, _fighter._playerID);
            if (!_blockList.Contains(2)) { _blockList.Add(2); }
        };
        _inputs.Fighter.UnBlock_2.performed += ctx => { if (_blockList.Contains(2)) { _blockList.Remove(2); } };
        _inputs.Fighter.Block_3.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.RightShoulder, _fighter._playerID);
            if (!_blockList.Contains(3)) { _blockList.Add(3); }
        };
        _inputs.Fighter.UnBlock_3.performed += ctx => { if (_blockList.Contains(3)) { _blockList.Remove(3); } };
        _inputs.Fighter.Block_4.performed += ctx =>
        {
            GamepadKeyEvents.InvokeGamepadKeyEvent(GamepadButton.RightTrigger, _fighter._playerID);
            if (!_blockList.Contains(4)) { _blockList.Add(4); }
        };
        _inputs.Fighter.UnBlock_4.performed += ctx => { if (_blockList.Contains(4)) { _blockList.Remove(4); } };

        //_inputs.Fighter.Enable();

        if (fighter._playerID == 1)
        {
            _inputs.devices = new[] { Gamepad.all.Count == 0 ? InputSystem.devices[0] : Gamepad.all[0] };
        }
        if (fighter._playerID == 2)
        {
            _inputs.devices = new[] { Gamepad.all.Count > 1 ? Gamepad.all[1] : (Gamepad.all.Count > 0 ? InputSystem.devices[0] : null) };
        }
    }

    bool PerformSpecial(InputAction action)
    {
        // Debug.LogWarning("PerformSpecial Timing:" + (Time.time - checkTime) + "specialTiming " + specialTiming);
        // Debug.LogWarning("specialDirection :" + specialDirection + "action " + action);
        if (Time.time - checkTime > specialTiming)
        {
            return false;
        }

        switch (specialDirection)
        {
            case SpecialDirection.Backward:
                if (action == _inputs.Fighter.LegAttack_2 && _fighter.Combat.CanDoSpecial(2))
                {
                    OnSpecalInput.Invoke(2);
                    return true;
                }
                if (action == _inputs.Fighter.HandAttack_2 && _fighter.Combat.CanDoSpecial(1))
                {
                    OnSpecalInput.Invoke(1);
                    return true;
                }
                break;
            case SpecialDirection.Forward:
                if (action == _inputs.Fighter.LegAttack && _fighter.Combat.CanDoSpecial(2))
                {
                    OnSpecalInput.Invoke(2);
                    return true;
                }
                if (action == _inputs.Fighter.HandAttack && _fighter.Combat.CanDoSpecial(1))
                {
                    OnSpecalInput.Invoke(1);
                    return true;
                }
                break;
        }
        return false;
    }
    private void OnEnable()
    {

    }
    private void OnDisable()
    {
        if (_inputs != null)
        {
            _inputs.Fighter.Disable();
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (_blockList.Count == 0 && _isBlocking)
        {
            _isBlocking = false;
            OnBlock.Invoke(false);
        }
        if (_blockList.Count > 0 && !_isBlocking)
        {
            OnBlock.Invoke(true);
            _isBlocking = true;

        }

    }

    private void FixedUpdate()
    {
        if (_moveLeft)
        {
            _isMoving = true;
            OnMove.Invoke(Direction.Left);
        }

        if (_moveRight)
        {
            _isMoving = true;
            OnMove.Invoke(Direction.Right);
        }

        if (_isMoving && !_moveLeft && !_moveRight)
        {
            _isMoving = false;
            OnStopMoving.Invoke();
        }

        OnCrouch.Invoke(_isCrouching);


    }

    public void AttackEnded()
    {
        OnAttackEnded.Invoke();
    }
    public void ResetBlock()
    {
        _blockList = new List<int>();
        OnBlock.Invoke(false);
    }
    public void ToggleControls(bool activate)
    {

        _moveLeft = false;
        _moveRight = false;
        _isMoving = false;
        _isCrouching = false;

        if (!activate)
        {
            _inputs.Disable();
            return;
        }
        if (activate)
        {   
            _inputs.Enable();

            if (_fighter._playerID == 1)
            {
                _inputs.devices = new[] { Gamepad.all.Count == 0 ? InputSystem.devices[0] : Gamepad.all[0] };
            }
            if (_fighter._playerID == 2)
            {
                _inputs.devices = new[] { Gamepad.all.Count > 1 ? Gamepad.all[1] : (Gamepad.all.Count > 0 ? InputSystem.devices[0] : null) };
            }


        }


    }


}