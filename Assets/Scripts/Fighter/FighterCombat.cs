using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterCombat : MonoBehaviour
{
    Fighter _fighter = null;
    private SpecialBase _specials = null;
    public SpecialBase Special => _specials;
    [SerializeField] float _handAttackDamage = 5f;
    public float HandAttackDamage => _handAttackDamage;
    [SerializeField] float _legAttackDamage = 5f;
    public float LegAttackDamage => _legAttackDamage;
    [SerializeField] float _upperCutAttackDamage = 10f;
    #region Combat Transforms
    [Header("air-coliders")]
    [SerializeField] Collider2D air_hand_coldier;
    public Collider2D Air_hand_coldier => air_hand_coldier;
    [SerializeField] Collider2D air_leg_coldier;
    public Collider2D Air_leg_coldier => air_leg_coldier;
    [Header("mid-hand")]
    [SerializeField] Transform _midHandAttack_Start_Position = null;
    [SerializeField] Transform _midHandAttack_End_Position = null;
    [Header("mid-leg")]
    [SerializeField] Transform _midLegAttack_Start_Position = null;
    [SerializeField] Transform _midLegAttack_End_Position = null;
    [Header("low-hand")]
    [SerializeField] Transform _lowHandAttack_Start_Position = null;
    [SerializeField] Transform _lowHandAttack_End_Position = null;
    [Header("low-leg")]
    [SerializeField] Transform _lowLegAttack_Start_Position = null;
    [SerializeField] Transform _lowLegAttack_End_Position = null;
    [Header("air-hand")]
    [SerializeField] Transform _airHandAttack_Start_Position = null;
    [SerializeField] Transform _airHandAttack_End_Position = null;
    [Header("air-leg")]
    [SerializeField] Transform _airLegAttack_Start_Position = null;
    [SerializeField] Transform _airLegAttack_End_Position = null;
    [Header("upper-cut")]
    [SerializeField] Transform _upperCutAttack_Start_Position = null;
    [SerializeField] Transform _upperCutAttack_End_Position = null;
    [Header("Specials")]
    [SerializeField] Transform _SpecialStart_1_Start_Position = null;

    [SerializeField] Transform _SpecialStart_2_Start_Position = null;
    #endregion /Combat Transforms
    [HideInInspector] public EventIntFloatAttackType OnAttack = new EventIntFloatAttackType();

    public void Init(Fighter fighter)
    {
        _fighter = fighter;
        _specials = fighter.gameObject.GetComponent<SpecialBase>();
        _specials.Init(fighter, this);

        fighter.OnLanding.AddListener((boolValue) =>
        {
            air_hand_coldier.enabled = false;
            air_leg_coldier.enabled = false;
        });
        fighter.OnHit.AddListener((hittype) =>
        {
            air_hand_coldier.enabled = false;
            air_leg_coldier.enabled = false;
        });
        fighter.OnKnockedDown.AddListener(() =>
        {
            air_hand_coldier.enabled = false;
            air_leg_coldier.enabled = false;
        });
    }

    public bool CanDoSpecial(int specialID)
    {
        return _specials.CanDoSpecial;
    }
    public void SpecialAttack(int type)
    {

        if (type == 1)
        {

            Vector3 spawnPos = _SpecialStart_1_Start_Position.transform.position;
            if (!_fighter.Inverted)
            {
                spawnPos = new Vector3(transform.position.x + (transform.position.x - _SpecialStart_1_Start_Position.position.x), _SpecialStart_1_Start_Position.position.y, transform.position.z);
            }
            _specials.Special_1(spawnPos);
            return;
        }
        if (type == 2)
        {

            Vector3 spawnPos = _SpecialStart_2_Start_Position.position;
            if (_fighter.Inverted)
            {
                spawnPos = new Vector3(transform.position.x + (transform.position.x - _SpecialStart_2_Start_Position.position.x), _SpecialStart_2_Start_Position.position.y, transform.position.z);
            }
            _specials.Special_2(spawnPos);
            return;
        }
    }
    void Attack(AttackType attack)
    {
        if (_fighter.IsDoingFatality)
        {
            return;
        }

        Vector2 originPosition = Vector2.zero;
        Vector2 endPosition = Vector2.zero;
        float damage = 0;

        switch (attack)
        {
            case AttackType.Hand_Mid:
                originPosition = _midHandAttack_Start_Position.position;
                endPosition = _midHandAttack_End_Position.position;
                damage = _handAttackDamage;
                break;
            case AttackType.Leg_Mid:
                originPosition = _midLegAttack_Start_Position.position;
                endPosition = _midLegAttack_End_Position.position;
                damage = _legAttackDamage;
                break;
            case AttackType.Hand_Low:
                originPosition = _lowHandAttack_Start_Position.position;
                endPosition = _lowHandAttack_End_Position.position;
                damage = _handAttackDamage;
                break;
            case AttackType.Leg_Low:
                originPosition = _lowLegAttack_Start_Position.position;
                endPosition = _lowLegAttack_End_Position.position;
                damage = _legAttackDamage;
                break;
            case AttackType.Hand_Air:
                originPosition = _airHandAttack_Start_Position.position;
                endPosition = _airHandAttack_End_Position.position;
                damage = _handAttackDamage;

                Vector3 newPosHand = air_hand_coldier.transform.localPosition;
                newPosHand.x = _fighter.Inverted ? -Mathf.Abs(newPosHand.x) : Mathf.Abs(newPosHand.x);
                air_hand_coldier.transform.localPosition = newPosHand;

                Vector3 scaleHand = air_hand_coldier.transform.localScale;
                scaleHand.x = _fighter.Inverted ? -Mathf.Abs(scaleHand.x) : Mathf.Abs(scaleHand.x);
                air_hand_coldier.transform.localScale = scaleHand;

                air_hand_coldier.enabled = true;
                return;
            case AttackType.Leg_Air:
                originPosition = _airLegAttack_Start_Position.position;
                endPosition = _airLegAttack_End_Position.position;
                damage = _legAttackDamage;

                Vector3 newPosLeg = air_leg_coldier.transform.localPosition;
                newPosLeg.x = _fighter.Inverted ? -Mathf.Abs(newPosLeg.x) : Mathf.Abs(newPosLeg.x);
                air_leg_coldier.transform.localPosition = newPosLeg;

                Vector3 scaleLeg = air_hand_coldier.transform.localScale;
                scaleLeg.x = _fighter.Inverted ? -Mathf.Abs(scaleLeg.x) : Mathf.Abs(scaleLeg.x);
                air_hand_coldier.transform.localScale = scaleLeg;

                air_leg_coldier.enabled = true;
                return; ;
            case AttackType.Hand_Low_UpperCut:
                originPosition = _upperCutAttack_Start_Position.position;
                endPosition = _upperCutAttack_End_Position.position;
                damage = _upperCutAttackDamage;
                break;

        }
        if (!_fighter.Inverted)
        {
            originPosition = new Vector2(transform.position.x + (transform.position.x - originPosition.x), originPosition.y);
            endPosition = new Vector2(transform.position.x + (transform.position.x - endPosition.x), endPosition.y);
        }
        Vector2 direction = endPosition - originPosition;
        float distance = direction.magnitude;
        LayerMask mask = _fighter._playerID == 1 ? LayerMask.GetMask("p2") : LayerMask.GetMask("p1");

        Debug.DrawRay(originPosition, direction, Color.red, 3f);
        RaycastHit2D hit = Physics2D.Raycast(originPosition, direction, distance, mask);

        if (hit.collider != null)
        {
#if UNITY_EDITOR
           // Debug.LogWarning(hit.collider.gameObject.name +" was hit by" + this.gameObject.name +" using "+ attack);
#endif
            OnAttack.Invoke(_fighter._playerID, damage, attack);
        }





    }

}