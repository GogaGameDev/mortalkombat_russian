using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class Malysheva_Special : SpecialBase
{
    [SerializeField] float _special_1_HealthRestore = 15f;
    [SerializeField] float _special_2_Damage = 10f;
    [SerializeField] Color stone_Color;
    Coroutine _special_1_Routine = null;
    Coroutine _special_2_Routine = null;
    
    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);

#if UNITY_EDITOR
        if (_fighter._playerID == 1)
        {
            // Fatality();
        }
#endif

    }

    #region Specials
    public override void ResetSpecials()
    {
        base.ResetSpecials();
        if (_special_1_Routine != null)
        {

            StopCoroutine(_special_1_Routine);
            _special_1_Routine = null;
            
        }

        if (_special_2_Routine != null)
        {
            StopCoroutine(_special_2_Routine);
            _special_2_Routine = null;
           
        }
    }
    public override void Special_1(Vector3 spawnPos)
    {

#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif

        if (!_canDoSpecial) {
            return;
        }
        base.Special_1(spawnPos);
        if (_special_1_Routine != null)
        {
            StopCoroutine(_special_1_Routine);
            _special_1_Routine = null;
        }
        _special_1_Routine = StartCoroutine(Special_1Routine(spawnPos));

        //_combat.
    }
    IEnumerator Special_1Routine(Vector3 spawnPos)
    {
        float currentHealth = _fighter.Health;
        float newValue = Mathf.Clamp(currentHealth + _special_1_HealthRestore, 0,100);
        _fighter.ChangeHealth(newValue);
        yield return new WaitForEndOfFrame();

    }

       
    
    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif

        if (!_canDoSpecial)
        {
            return;
        }

        base.Special_2(spawnPos);

        if (_special_2_Routine != null) 
        {
            StopCoroutine(_special_2_Routine);
            _special_2_Routine = null;
        }

        _special_2_Routine = StartCoroutine(Special_2Routine(spawnPos));
    }


    IEnumerator Special_2Routine(Vector3 spawnPos)
    {


        Vector3 origin = spawnPos;
        Vector3 direction = _fighter.Inverted ? -transform.right : transform.right;
        float distance = 1f;
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Debug.DrawRay(origin, direction.normalized * distance, Color.red, 1f);
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, mask);

        if (hit.collider != null)
        {
            Fighter f = hit.collider.gameObject.GetComponent<Fighter>();

            if (f.TakeDamage(_special_2_Damage, AttackType.Special) == 0)
            {
                yield break;
            }
            f.Inputs.ToggleControls(false);
            f.ResetAllActiveStatus();
            yield return new WaitForSeconds(0.1f);
            f.Animation.SetState("mid_reaction_anim");
            yield return new WaitForSeconds(0.1f);
            f.Animation.SetState("malysheva_special_2_reaction");
            f.transform.DOMoveX(f.transform.position.x + 0.1f, 0.075f).SetLoops(-1, LoopType.Yoyo);
            yield return new WaitForSeconds(0.25f);
            f.transform.DOKill();
            yield return new WaitForSeconds(1f);
            f.GetComponent<SpriteRenderer>().color = Color.white;
            if (f.Health > 0)
            {
                f.Animation.SetState("IDL");
            }
            else 
            {
                f.Animation.SetState("FinalDeath");
            }
          
            f.Inputs.ToggleControls(true);

        }

        ResetSpecials();

    }

    #endregion /Specials
    #region Fatality
    [Space(10)]
    [Header("Fatality")]
    [SerializeField] GameObject bloodFXPrefab = null;
    public override void Fatality()
    {
        base.Fatality();
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }

    IEnumerator FatalityRoutine()
    {
        OnFatalityStarted.Invoke();
        _fighter.Inputs.ToggleControls(false);
        // move to position
        Vector3 pos = _fighter.Opponent.position - (_fighter.Opponent.position - _fighter.transform.position).normalized * 1.1f;
        float moveDelay = Mathf.Abs(transform.position.x - pos.x) / 1.1f;
        _fighter.MoveToPosition(pos, moveDelay);
     
        yield return new WaitForSeconds(moveDelay);
        // code here
        _fighter.Animation.SetState("fatality_pose");
        yield return new WaitForSeconds(0.25f);
        _fighter.Opponent.GetComponent<Fighter>().Animation.SetState("malysheva_fatality_reaction");
        yield return new WaitForSeconds(0.75f);
        Instantiate(bloodFXPrefab, _fighter.Opponent.position, Quaternion.identity);
        yield return new WaitForSeconds(0.75f);
     

        OnFatalityFinished.Invoke();

    }

    #endregion /Fatality
}