﻿using UnityEngine;
using UnityEngine.Audio;
using AudioSettings = Audio.AudioSettings;

namespace Audio
{
    public class AudioMixerController : MonoBehaviour
    {
        [SerializeField] private AudioMixer _audioMixer;
        [SerializeField] private float MuteVolume;

        private void OnEnable()
        {
            AudioSettings.SoundChanged += OnSoundChanged;
            AudioSettings.MusicChanged += OnMusicChanged;
        }

        private void OnDisable()
        {
            AudioSettings.SoundChanged -= OnSoundChanged;
            AudioSettings.MusicChanged -= OnMusicChanged;
        }

        private void OnMusicChanged()
        {
            _audioMixer.SetFloat("MusicVolume", Volume(AudioSettings.MusicIsOn));
        }

        private void OnSoundChanged()
        {
            _audioMixer.SetFloat("SoundVolume", Volume(AudioSettings.SoundIsOn));
        }

        private float Volume(bool isOn) => isOn ? 0 : MuteVolume;

        private void Awake()
        {
            OnMusicChanged();
            OnSoundChanged();
        }
    }
}