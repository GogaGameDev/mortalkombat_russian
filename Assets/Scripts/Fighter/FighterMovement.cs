using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public partial class Fighter
{
    [SerializeField] float landYpos = 0.159f;
    public float LandingPosY => landYpos;
    private void Start()
    {

    }
    public void GetLandingPos()
    {
        landYpos = transform.position.y;
    }
    void Move(Direction dir)
    {

        _moveDirection = dir;

        if (_isAttacking || _isBlocking || _isCrouching || _isKnockedDown || IsDoingSpecial || _isStunned || _isDoingFatality || !IsOnGround)
        {
            return;
        }
        Vector3 direction = Vector3.zero;
        bool backWard = false;

        switch (dir)
        {
            case Direction.Left:
                direction = -transform.right;
                backWard = _spriteRenderer.flipX ? false : true;
                break;

            case Direction.Right:
                direction = transform.right;
                backWard = _spriteRenderer.flipX ? true : false;
                break;
        }
        Vector3 newPos = rb.position;
        newPos += direction * _moveSpeed * Time.fixedDeltaTime;
        rb.position = CorrectPos(newPos, rb.position);
        _isMoving = true;
        // Debug.Log("Backward " + backWard);
        OnMoveStart.Invoke(backWard);


    }
    IEnumerator JumpInDirection()
    {
        Vector3 nextPosUp = rb.position;
        Vector3 nextPosDown = rb.position;
        float distance = 1.5f;
        float duration = 0.5f;
        bool backWard = false;
        switch (_moveDirection)
        {
            case Direction.Left:
                nextPosUp = transform.position - transform.right * distance * 0.75f + transform.up * jumpHeight;
                nextPosDown = transform.position - transform.right * distance;
                backWard = _spriteRenderer.flipX ? false : true;
                break;
            case Direction.Right:
                nextPosUp = transform.position + transform.right * distance * 0.75f + transform.up * jumpHeight;
                nextPosDown = transform.position + transform.right * distance;
                backWard = _spriteRenderer.flipX ? true : false;
                break;
        }

        nextPosUp = CorrectPos(nextPosUp, transform.position);
        UpdateColiderTrigger(true);
        OnSomersault.Invoke(true, backWard, _isOnGround);
        _isOnGround = false;
        transform.DOMove(nextPosUp, duration);
        yield return new WaitForSeconds(duration);
        nextPosDown = CorrectPos(nextPosDown, transform.position);
        transform.DOMove(nextPosDown, duration);
        yield return new WaitForSeconds(duration);
        UpdateColiderTrigger(false);
        StartCoroutine(Landing());

    }
    IEnumerator Jump()
    {

        if (!_isOnGround || _isBlocking || _isAttacking || _isKnockedDown || IsDoingSpecial || _isStunned || _isDoingFatality)
        {
            yield break;
        }
        OnJump.Invoke();
        _isOnGround = false;
        if (_isMoving)
        {

            _jumpDirectionRoutine = StartCoroutine(JumpInDirection());
            yield break;
        }

        //Debug.Log("Jump");

        rb.AddForce(Vector2.up * 4f, ForceMode2D.Impulse);
        //  _state = FighterState.Jump;
        // transform.DOMove(transform.position + transform.up * jumpHeight * 0.8f, 0.5f);
        // yield return new WaitForSeconds(0.5f);
        // transform.DOMove(new Vector3(transform.position.x, 0.159f, transform.position.z), 0.5f);
        // yield return new WaitForSeconds(0.5f);
        //OnLanding.Invoke(_isMoving);
        //_animations.AttackAnimationStop();
        //_isOnGround = true;
        //yield return new WaitForSeconds(0.15f);
        //_airAttack = false;
        //_isAttacking = false;

        // _state = FighterState.IDL;

    }

    public void StopMovement()
    {
        _isStunned = true;
        OnMoveStop.Invoke();
        _inputs.ToggleControls(false);
    }
   public void PushBack(float distance, float height, float time)
    {
        Vector3 endPos = new Vector3(transform.position.x, landYpos, transform.position.z) - (Inverted ? -transform.right : transform.right) * distance;

        if (!_isOnGround)
        {
            if (_jumpRoutine != null)
            {
                StopCoroutine(_jumpRoutine);
            };
            if (_jumpDirectionRoutine != null)
            {
                StopCoroutine(_jumpDirectionRoutine);
            }

        }


        endPos = CorrectPos(endPos, transform.position);
        transform.DOKill();
        // rb.AddForce(new Vector2(1f, 5f), ForceMode2D.Impulse);
        transform.DOJump(endPos, height, 1, time);
    }
    public Vector3 CorrectPos(Vector3 newPos, Vector3 oldPos)
    {
        float oldDistance = Mathf.Abs(oldPos.x - _opponentPos.transform.position.x);
        float newdistance = Mathf.Abs(newPos.x - _opponentPos.transform.position.x);
        bool directionToOpponent = newdistance < oldDistance;

        if (newdistance > 2.5f && !directionToOpponent)
        {
            newPos = new Vector3(oldPos.x, newPos.y, newPos.z);
        }
        float posX = Mathf.Clamp(newPos.x, -4.25f, 4.25f);
        return new Vector3(posX, newPos.y, newPos.z); ;
    }

    public void UpdateColiderOnChangeSides()
    {
        float xOffset = Inverted ? 0.1147221f : -0.03510916f;
        foreach (var item in _colider)
        {
            item.offset = new Vector2(0, item.offset.y);
        }
    }
    public void MoveToPosition(Vector3 pos, float duration)
    {
        Direction dir = pos.x > transform.position.x ? Direction.Right : Direction.Left;

        bool backward = false;
        if ((dir == Direction.Right && Inverted) || (dir == Direction.Left && !Inverted))
        {
            backward = true;
        }
        OnMoveStart.Invoke(backward);
        transform.DOMove(pos, duration).OnComplete(() =>
        {
            OnMoveStop.Invoke();
        });

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //  Debug.Log("collision.gameObject.layer " + collision.gameObject.layer + "=" + "LayerMask.NameToLayer(Ground)" + LayerMask.NameToLayer("Ground"));
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            StartCoroutine(Landing());
        }
    }
    IEnumerator Landing()
    {
        if (_isOnGround)
        {
            yield break;
        }

        _airAttack = false;
        _isAttacking = false;
        if (_isStunned)
        {
            OnStunn.Invoke();
            yield break;
        }
        if (!_isCrouching)
        {
            OnLanding.Invoke(_isMoving);
        }
        _animations.AttackAnimationStop();
        _inputs.ResetBlock();
        yield return new WaitForSeconds(0.1f);
        _isOnGround = true;
        _airAttack = false;
        _isAttacking = false;
    }

}