﻿using System;
using Audio.SoundPlayers;
using UnityEngine;

namespace Audio
{
    public class OnGameOverPlay : OnEventPlay
    {
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private WinnerSound[] _winnerSounds;
        protected override void SubscribeToEvent()
        {
            _gameManager.OnGameOver.AddListener(PlayWinner);
        }

        private void PlayWinner()
        {
            Fighter winner;
            if (_gameManager._playerFighters[0].IsStunned)
                winner = _gameManager._playerFighters[1];
            else if (_gameManager._playerFighters[1].IsStunned)
                winner = _gameManager._playerFighters[0];
            else return;

            foreach (var winnerSound in _winnerSounds)
            {
                if (winnerSound.Fighter.FighterName == winner.FighterName)
                {
                    print("PLAY WINNER");
                    ((WinSoundPlayer) _soundPlayer).SetWinner(winnerSound.FighterIndex);
                    PlaySound();
                    break;
                }
            }

        }

        [Serializable]
        public struct WinnerSound
        {
            public Fighter Fighter;
            public int FighterIndex;
        }
    }
}