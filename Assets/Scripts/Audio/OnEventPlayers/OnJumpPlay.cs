﻿using UnityEngine;

namespace Audio
{
    public class OnJumpPlay : OnEventPlay
    {
        [SerializeField] private Fighter _fighter;

        protected override void SubscribeToEvent()
        {
            _fighter.OnJump.AddListener(PlaySound);
        }
    }
}