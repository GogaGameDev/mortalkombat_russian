﻿using Audio.SoundPlayers;
using UnityEngine;

namespace Audio
{
    public abstract class OnEventPlay : MonoBehaviour
    {
        protected abstract void SubscribeToEvent();

        protected virtual void PlaySound()
        {
            _soundPlayer.Play();
        }
        private void Start()
        {
            _soundPlayer = GetComponent<SoundPlayer>();
            SubscribeToEvent();
        }

        protected SoundPlayer _soundPlayer;
    }
}