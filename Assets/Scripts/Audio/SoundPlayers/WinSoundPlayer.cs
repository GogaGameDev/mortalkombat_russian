﻿using DG.Tweening;
using UnityEngine;

namespace Audio.SoundPlayers
{
    /// <summary>
    /// Provides a method for playing "[character name] wins" phrase
    /// </summary>
    public class WinSoundPlayer : SoundPlayer
    {
        [SerializeField] private AudioClip[] _characterNames;
        [SerializeField] private AudioClip _winsSound;
        [SerializeField] private float _delay = 1f;

        public void SetWinner(int id)
        {
            _characterNameID = id;
        }
        public override void Play()
        {
            DOTween.Sequence().AppendCallback(() => PlayOnce(_characterNames[_characterNameID])).AppendInterval(_delay)
                .AppendCallback(() => PlayOnce(_winsSound));
        }

        private int _characterNameID;
    }
}