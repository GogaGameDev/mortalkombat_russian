﻿using Audio.SoundPlayers;
using UnityEngine;

namespace Audio
{
    public class OnFinishHimPlay : OnEventPlay
    {
        [SerializeField] private GameManager _gameManager;
        protected override void SubscribeToEvent()
        {
            _gameManager.OnEndgameStarted.AddListener(PlayFinishSound);
        }

        private void PlayFinishSound()
        {
            ((FinishSoundPlayer) _soundPlayer).SetGender(_gameManager.FinishGender);
            PlaySound();
        }
    }
}