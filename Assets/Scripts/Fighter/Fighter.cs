using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;


public partial class Fighter : MonoBehaviour
{
    [SerializeField] Transform _opponentPos = null;
    private Vector3 InitPos = Vector3.zero;
    [SerializeField] private CapsuleCollider2D[] _colider = null;
    [SerializeField] private float _moveSpeed = 0;

    [SerializeField] private bool _isOnGround = true;
    [SerializeField] private bool _isMoving = false;
    [SerializeField] private bool _isBlocking = false;
    [SerializeField] private bool _isCrouching = false;
    [SerializeField] private bool _isKnockedDown = false;
    [SerializeField] private bool _isDoingFatality = false;
    [SerializeField] private bool _isStunned = false;
    [SerializeField] private bool _isDead = false;
    [SerializeField] private bool _airAttack = false;

    [SerializeField] private string _fighterName = "";
    [SerializeField] private string _fighterID = "";
    [SerializeField] private Direction _moveDirection = Direction.Left;
    [SerializeField] SpriteRenderer _spriteRenderer = null;

    Coroutine _jumpRoutine = null;
    Coroutine _jumpDirectionRoutine = null;
    Coroutine _stunnRoutine = null;
    Coroutine _knockDownRoutine = null;
    Rigidbody2D rb;
    public Rigidbody2D RB => rb;
    private FighterCombat _combat = null;
    private FighterInputs _inputs = null;
    private FighterAnimation _animations = null;
    public string FighterID=>_fighterID;
    public FighterInputs Inputs => _inputs;
    public FighterCombat Combat => _combat;
    public FighterAnimation Animation => _animations;
    [SerializeField] float _health = 100;
    private float jumpHeight = 1f;
    public float Health => _health;
    public string FighterName => _fighterName;
    public bool IsBlocking => _isBlocking;
    public bool IsDoingFatality => _isDoingFatality;
    public bool IsOnGround => _isOnGround;
    public bool IsDoingSpecial;
    public bool IsKnockedDown => _isKnockedDown;
    public bool _isAttacking = false;
    public bool Inverted { get { return _spriteRenderer.flipX; } }
    public bool IsDead => _isDead;
    public bool IsStunned => _isStunned;
    public bool IsUnderSpecial;
    public Transform Opponent => _opponentPos;

    public int _playerID = 1;

    public Gender _gender = Gender.Male;

    #region Events
    [HideInInspector] public UnityEvent OnJump;
    [HideInInspector] public EventBoolBoolBool OnSomersault = new EventBoolBoolBool();
    [HideInInspector] public EventBool OnLanding = new EventBool();
    [HideInInspector] public EventBool OnCrouch = new EventBool();
    [HideInInspector] public EventBool OnBlock = new EventBool();
    [HideInInspector] public EventBool OnMoveStart = new EventBool();
    [HideInInspector] public EventBool OnInvertedSides = new EventBool();
    [HideInInspector] public UnityEvent OnMoveStop;
    [HideInInspector] public EventAttack OnAttack = new EventAttack();
    [HideInInspector] public EventHitType OnHit = new EventHitType();
    [HideInInspector] public EventInt OnTakingDamage = new EventInt();
    [HideInInspector] public UnityEvent OnGetup;
    [HideInInspector] public EventIntFloat OnHealthChange = new EventIntFloat();
    [HideInInspector] public EventInt OnDeath = new EventInt();
    [HideInInspector] public UnityEvent OnStunn = new UnityEvent();
    [HideInInspector] public UnityEvent OnFinalDeath;
    [HideInInspector] public UnityEvent OnBlockBreak;
    [HideInInspector] public UnityEvent ZeroHealh;
    [HideInInspector] public UnityEvent OnKnockedDown;
    #endregion /Events

    public void Init(int playerID, Transform opponent)
    {
        InitPos = transform.position;
        rb = this.GetComponent<Rigidbody2D>();
        _playerID = playerID;
        this.gameObject.layer = playerID == 1 ? LayerMask.NameToLayer("p1") : LayerMask.NameToLayer("p2");
        _spriteRenderer.sortingOrder = playerID == 1 ? 1 : 2;
        _opponentPos = opponent;
        _spriteRenderer.flipX = playerID == 1 ? false : true;
        _inputs = GetComponent<FighterInputs>();
        _inputs.OnMove.AddListener(Move);

        _inputs.OnFatality.AddListener(() =>
        {
            if (!_isOnGround || _isCrouching || IsDoingSpecial || _health <= 0)
            {
                return;
            }
            _isDoingFatality = true;
            _inputs.ToggleControls(false);
            _combat.Special.Fatality();
        });

        _inputs.OnStopMoving.AddListener(() =>
        {
            _isMoving = false;
            //  _state = FighterState.IDL;
            OnSomersault.Invoke(_isMoving, !_spriteRenderer.flipX, _isOnGround);
            OnMoveStop.Invoke();
            //UpdateColiderSize();
        });
        _inputs.OnCrouch.AddListener((isCrouching) =>
        {
            if (!isCrouching && _isCrouching != isCrouching)
            {
                _isCrouching = isCrouching;
                Debug.Log("OnCrouch " + _isCrouching);
                OnCrouch.Invoke(_isCrouching);

                return;
            }
            if (IsDoingSpecial || !_isOnGround || _isCrouching == isCrouching || _isAttacking || IsStunned || _isKnockedDown || transform.position.y > 0.161f)
            {
                return;
            }
            _isCrouching = isCrouching;
            if (_isCrouching)
            {
                OnMoveStop.Invoke();
                // _state = FighterState.Crouch;
            }
            Debug.Log("OnCrouch " + _isCrouching);
            OnCrouch.Invoke(_isCrouching);

        });

        _inputs.OnJump.AddListener(() => { _jumpRoutine = StartCoroutine(Jump()); });
        _inputs.OnAttackHand.AddListener(HandAttack);
        _inputs.OnAttackLeg.AddListener(LegAttack);

        _inputs.OnBlock.AddListener((isblocking) =>
        {
            if (IsDoingSpecial || _isAttacking || !_isOnGround || _isStunned)
            {
                return;
            }
            _isBlocking = isblocking;
            if (_isBlocking)
            {
                OnMoveStop.Invoke();
            }
            _isMoving = false;
            OnBlock.Invoke(isblocking);
            //  _state = FighterState.Block;
        });

        _inputs.OnAttackEnded.AddListener(() =>
        {
            _isAttacking = false;

        });

        _combat = this.GetComponent<FighterCombat>();
        _combat.Init(this);
        _combat.Special.OnFatalityFinished.AddListener(() =>
        {
            _isDoingFatality = false;
        });
        _animations = this.GetComponent<FighterAnimation>();

        _colider = this.GetComponents<CapsuleCollider2D>();
        UpdateColiderTrigger(false);
        UpdateColiderOnChangeSides();
    }

    private void Update()
    {
        if (!_isBlocking && _isOnGround && !IsDoingSpecial && !_isKnockedDown && !IsUnderSpecial)
        {
            bool newValue = _opponentPos.position.x < transform.position.x ? true : false;
            if (newValue != _spriteRenderer.flipX)
            {
                UpdateColiderOnChangeSides();


            }
            _spriteRenderer.flipX = newValue;
        }
    }

    void HandAttack(int handType)
    {

        if (_isAttacking || _isBlocking || _isKnockedDown || IsDoingSpecial || _isStunned || _isDoingFatality || _airAttack)
        {

            return;
        }
        _isAttacking = true;
        _isMoving = false;
        OnMoveStop.Invoke();
        //   _state = FighterState.Attack;
        if (!_isOnGround && !_airAttack)
        {
            UpdateSide();
            UpdateColiderTrigger(false);
            // Debug.LogWarning("Attadk Hand air" + transform.position.y);
            OnAttack.Invoke(AttackType.Hand_Air);
            _airAttack = true;
            return;
        }

        if (!_isOnGround)
        {
            return;

        }
        if (_isCrouching)
        {
            OnAttack.Invoke(handType == 2 ? AttackType.Hand_Low_UpperCut : AttackType.Hand_Low);
            Debug.Log(handType == 2 ? AttackType.Hand_Low_UpperCut : AttackType.Hand_Low);
            return;
        }

        OnAttack.Invoke(AttackType.Hand_Mid);

        return;

    }
    void LegAttack()
    {
        if (_isAttacking || _isBlocking || _isKnockedDown || IsDoingSpecial || _isStunned || _isDoingFatality)
        {
            return;
        }
        _isAttacking = true;
        //  _state = FighterState.Attack;
        if (!_isOnGround && !_airAttack)
        {
            UpdateSide();
            UpdateColiderTrigger(false);
            OnAttack.Invoke(AttackType.Leg_Air);
            _airAttack = true;
            return;
        }
        if (!_isOnGround)
        {
            return;

        }
        if (_isCrouching)
        {
            OnAttack.Invoke(AttackType.Leg_Low);
            return;
        }

        OnAttack.Invoke(AttackType.Leg_Mid);

    }

    public void UpdateColider(ColiderParams coliderParams)
    {
        //_colider.offset = coliderParams.offset;
        //_colider.size = coliderParams.size;

    }
    public void ResetAllActiveStatus(float delay = 0)
    {
        StartCoroutine(ResetAllActiveStatusRoutine(delay));

    }
    IEnumerator ResetAllActiveStatusRoutine(float delay)
    {
        yield return new WaitForSeconds(delay);

        Debug.Log(_fighterName + "ResetAllActiveStatus");
        if (IsUnderSpecial)
        {
            yield break;
        }

        if (_jumpDirectionRoutine != null)
        {
            //  StopCoroutine(_jumpDirectionRoutine);
        }
        if (_jumpRoutine != null)
        {
            //StopCoroutine(_jumpRoutine);
        }
        Combat.Special.ResetSpecials();
        Animation.ResetAnimations();
        Animation.AttackAnimationStop();

        _isOnGround = true;
        _isCrouching = false;
        _isBlocking = false;
        _isMoving = false;
        OnMoveStop.Invoke();
        UpdateColiderEnable(true);
        if (_health > 0)
        {
            Animation.SetState("IDL");
        }
        yield return new WaitForSeconds(0.25f);
        _isAttacking = false;
    }
    public void DoingSpecial(string isDoingSpecial)
    {
        //Trigger from special animation
        IsDoingSpecial = bool.Parse(isDoingSpecial);

    }
    IEnumerator KnockedDownRoutine(float stunnTime)
    {
        UpdateSide();
        _inputs.ToggleControls(false);
        ResetAllActiveStatus();
        UpdateColiderEnable(false);
        _isKnockedDown = true;
        yield return new WaitForSeconds(0.1f);
        _animations.SetState("reaction_uppercut_anim");
        OnKnockedDown.Invoke();
        yield return new WaitForSeconds(stunnTime);
        if (Health > 0)
        {
            OnGetup.Invoke();
        }
        else
        {
            yield break;
        }
        yield return new WaitForSeconds(0.5f);
        _isKnockedDown = false;
        _isOnGround = true;
        _inputs.ToggleControls(true);
        UpdateColiderEnable(true);

    }

    public void UpdateSide()
    {
        _spriteRenderer.flipX = _opponentPos.position.x < transform.position.x ? true : false;
    }
    bool Blocked(AttackType type)
    {
        if (_isBlocking)
        {
            if (_spriteRenderer.flipX && transform.position.x < _opponentPos.position.x)
            {
                _isBlocking = false;
                OnBlock.Invoke(false);
                OnBlockBreak.Invoke();
                return false;
            }
            if (!_spriteRenderer.flipX && transform.position.x > _opponentPos.position.x)
            {
                _isBlocking = false;
                OnBlock.Invoke(false);
                OnBlockBreak.Invoke();
                return false;
            }
        }

        switch (type)
        {
            case AttackType.Hand_Air:
                return _isBlocking;
            case AttackType.Hand_Mid:
                return _isBlocking;
            case AttackType.Hand_Low:
                return _isBlocking;
            case AttackType.Leg_Air:
                return _isBlocking;
            case AttackType.Leg_Mid:
                return _isBlocking;
            case AttackType.Leg_Low:
                return _isBlocking;
            case AttackType.Special:
                return _isBlocking;
            case AttackType.Hand_Low_UpperCut:
                return false;
        }

        return true;

    }
    public float TakeDamage(float damage, AttackType type)
    {

        if (_isStunned && _health == 0)
        {
            if (!_isDead && !GameManager.IsEndGame())
            {
                _isDead = true;
                OnFinalDeath.Invoke();
            }

            return 0;
        }

        if (GameManager.gameOver || _isKnockedDown)
        {
            return 0;
        }


        bool blocked = Blocked(type);
        _isAttacking = false;



        //for Animations
        if (type == AttackType.Hand_Low_UpperCut || type == AttackType.Leg_Air)
        {

            if (_knockDownRoutine != null)
            {
                StopCoroutine(_knockDownRoutine);
            }
            _knockDownRoutine = StartCoroutine(KnockedDownRoutine(1f));
            PushBack(1f, 1.25f, 0.75f);
        }
        else
        {
            OnHit.Invoke(_isCrouching ? HitType.Low : HitType.Mid);
            if (!IsOnGround)
            {
                if (_knockDownRoutine != null)
                {
                    StopCoroutine(_knockDownRoutine);
                }

                _knockDownRoutine = StartCoroutine(KnockedDownRoutine(1f));
                PushBack(1f, 1.25f, 0.75f);
            }



        }


        damage = blocked ? 0 : damage;

        IsDoingSpecial = false;
        _health = Mathf.Clamp(_health - damage, 0, _health);
        OnHealthChange.Invoke(_playerID, _health);

        if (_health == 0)
        {
            ZeroHealh.Invoke(); // for opponent stop attack 

            Stunn(type == AttackType.Hand_Low_UpperCut ? 1.5f : 0, true, true);

        }

        if (damage > 0 || _health == 0)
        {
            OnTakingDamage.Invoke(_playerID);
        }

        if (_health > 0 && _stunnRoutine != null)
        {
            StopCoroutine(_stunnRoutine);
            _isStunned = false;
            _isKnockedDown = false;

        }
        return damage;
    }
    public void Stunn(float delay, bool recovery = true, bool death = false)
    {

        if (death)
        {
            _stunnRoutine = StartCoroutine(StunnRoutineDeath(delay));
        }
        else
        {
            _stunnRoutine = StartCoroutine(StunnRoutine(delay, recovery));
        }

    }

    IEnumerator StunnRoutineDeath(float delay)
    {

        ResetAllActiveStatus();
        _isStunned = true;
        yield return new WaitForSeconds(delay);
        _health = 0;
        OnHealthChange.Invoke(_playerID, _health);

        if (_isKnockedDown && GameManager.IsEndGame())
        {
            OnGetup.Invoke();
            yield return new WaitForSeconds(0.75f);
            _isKnockedDown = false;

        }
        OnDeath.Invoke(_playerID);
        _inputs.ToggleControls(false);

        if (transform.position.y != landYpos)
        {

            Vector3 newPos = transform.position;
            newPos.y = landYpos;
            transform.DOKill();
            transform.DOMove(newPos, 0.2f);
        }
        if (!GameManager.IsEndGame())
        {
            OnFinalDeath.Invoke();
        }
    }
    IEnumerator StunnRoutine(float delay, bool recovery = true)
    {
        if (_health == 0)
        {
            yield break;
        }
        _inputs.ToggleControls(false);
        OnStunn.Invoke();
        _isStunned = true;
        yield return new WaitForSeconds(delay);

        if (recovery)
        {
            Animation.SetTrigger("IDL");
            _inputs.ToggleControls(true);
            _isStunned = false;
        }

    }
    public void ChangeHealth(float newHP)
    {
        _health = newHP;
        OnHealthChange.Invoke(_playerID, _health);
    }
    public void Reset()
    {
        _health = 100;
        OnHealthChange.Invoke(_playerID, _health);

        transform.position = InitPos;
        _inputs.ToggleControls(false);
        Animation.SetBool("stunn_bool", false);
        Animation.SetBool("crouch_bool", false);
        Animation.ResetTrigger("final_death_trigger");
        _isDead = false;
        _isStunned = false;
        _isCrouching = false;
        _isMoving = false;
        IsDoingSpecial = false;
        _isKnockedDown = false;
        OnMoveStop.Invoke();
        UpdateColiderEnable(true);
        _animations.ResetAnimations();
        _animations.SetTrigger("IDL");

    }
    public int SetSortLayer(int layer)
    {
        int previos = _spriteRenderer.sortingOrder;
        _spriteRenderer.sortingOrder = layer;

        return previos;
    }
    public void SetColor(Color col)
    {
        _spriteRenderer.color = col;
    }

    public void UpdateColiderTrigger(bool isTrigger)
    {

        foreach (var item in _colider)
        {
            item.isTrigger = isTrigger;
        }
    }
    public void UpdateColiderEnable(bool enable)
    {
        //  rb.constraints = !enable ? (RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation) : RigidbodyConstraints2D.FreezeRotation;
        foreach (var item in _colider)
        {
            item.enabled = enable;
            item.isTrigger = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        switch (collision.gameObject.tag)
        {
            case "hand_air_attack":
                TakeDamage(Combat.HandAttackDamage, AttackType.Hand_Air);
                collision.enabled = false;
                break;
            case "leg_air_attack":
                TakeDamage(Combat.LegAttackDamage, AttackType.Leg_Air);
                collision.enabled = false;
                break;
        }
    }
}