using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class KirkorovSpecial : SpecialBase
{
    [SerializeField] float _special_1_Damage = 10f;
    [SerializeField] float _special_2_Damage = 10f;
    [SerializeField] float _frozenTime = 1.5f;
    [SerializeField] Color _frozenColor = Color.blue;
    [SerializeField] Projectile _featherPrefab = null;
    [SerializeField] Projectile _SnowBallPrefab = null;
    Coroutine _frozenRoutine = null;
    GameObject projectileObj = null;


    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);

#if UNITY_EDITOR
        if (_fighter._playerID==1) 
        {
           // Fatality();
        }
#endif
    }

    #region Specials
    public override void Special_1(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif


        if (!CanDoSpecial)
        {
            return;
        }
        base.Special_1(spawnPos);

        Projectile feather = Instantiate<Projectile>(_featherPrefab, spawnPos, Quaternion.identity);
        feather.InitThrow(_fighter.Inverted, _fighter);
        feather._OnHit.AddListener(OnHit_Special_1);
        projectileObj = feather.gameObject;

        //_combat.
    }

    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif

        if (!CanDoSpecial)
        {
            return;
        }
        base.Special_2(spawnPos);
        Projectile snowball = Instantiate<Projectile>(_SnowBallPrefab, spawnPos, Quaternion.identity);
        snowball.InitThrow(_fighter.Inverted, _fighter);
        snowball._OnHit.AddListener(OnHit_Special_2);
        projectileObj = snowball.gameObject;


    }

    void OnHit_Special_1(GameObject obj)
    {
        if (obj == this.gameObject)
        {
            return;
        }
        _combat.OnAttack.Invoke(_fighter._playerID, _special_1_Damage, AttackType.Special);
        if (projectileObj != null)
        {
            Destroy(projectileObj.gameObject, 0.25f);

        }
    }
    void OnHit_Special_2(GameObject obj)
    {
        // snowball
        if (obj == this.gameObject)
        {
            return;
        }
        _combat.OnAttack.Invoke(_fighter._playerID, _special_2_Damage, AttackType.Special);

        if (projectileObj != null)
        {
            Destroy(projectileObj.gameObject, 0.25f);

        }
        Fighter f = obj.GetComponent<Fighter>();
        if (_frozenRoutine != null)
        {
            StopCoroutine(_frozenRoutine);
            Unfreeze(f);
        }
        else
        {

            StartCoroutine(FrozenRoutine(f));
        }
    }
    IEnumerator FrozenRoutine(Fighter f)
    {
        if (f.IsBlocking)
        {
            _frozenRoutine = null;
            yield break;
        }
        yield return new WaitForSeconds(0.1f);
        if (f.Health <= 0)
        {
            yield break;
        }
        //f.IsUnderSpecial = true;
        f.gameObject.GetComponent<Animator>().enabled = false;
        f.SetColor(_frozenColor);
        f.gameObject.GetComponent<FighterInputs>().ToggleControls(false);
        yield return new WaitForSeconds(_frozenTime);
        Unfreeze(f);
        f.ResetAllActiveStatus();
    }
    void Unfreeze(Fighter f)
    {
        f.gameObject.GetComponent<Animator>().enabled = true;
        f.SetColor(Color.white);
        f.gameObject.GetComponent<FighterInputs>().ToggleControls(true);
       // f.IsUnderSpecial = false;
        if (f.Health <= 0)
        {
            f.Stunn(0, false, true);
        }
    }
    #endregion /Specials
    #region Fatality
    [Space(10)]
    [Header("Fatality")]
    [SerializeField] Image _whiteBG;
    [SerializeField] Animator fighterOponentAnimator;
    [SerializeField] GameObject fatalityObject;
    [SerializeField] Image pickImage = null;
    [SerializeField] Sprite pickWithBlood = null;
    [SerializeField] GameObject bloodFXPrefab = null;
    public override void Fatality()
    {
        base.Fatality();
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }
    IEnumerator FatalityRoutine()
    {
        OnFatalityStarted.Invoke();
        yield return new WaitForSeconds(0.5f);

        Fighter f = _fighter.Opponent.GetComponent<Fighter>();
        f.ResetAllActiveStatus();
        yield return new WaitForSeconds(0.1f);
        f.RB.constraints = RigidbodyConstraints2D.FreezeAll;
        _fighter.RB.constraints = RigidbodyConstraints2D.FreezeAll;
        f.UpdateColiderEnable(false);
        Vector3 pos = f.transform.position - (f.transform.position - _fighter.transform.position).normalized * 0.5f;
        Direction dir = pos.x > transform.position.x ? Direction.Right : Direction.Left;
        bool backward = false;
        if ((dir == Direction.Right && _fighter.Inverted) || (dir == Direction.Left && !_fighter.Inverted))
        {
            backward = true;
        }
        _fighter.OnMoveStart.Invoke(backward);
        float moveDelay = Mathf.Abs(_fighter.transform.position.x - pos.x) / 1.1f;
        _fighter.transform.DOMove(pos, moveDelay);
        _fighter.Animation.SetBool(_fighter.Inverted ? "walking_backward_bool" : "walking_bool", true);
        yield return new WaitForSeconds(moveDelay);
        _fighter.OnMoveStop.Invoke();
        _fighter.OnAttack.Invoke(AttackType.Hand_Low_UpperCut);
        yield return new WaitForSeconds(0.1f);
        f.Animation.SetState("FallingState");
        _fighter.Opponent.transform.DOKill();
        _fighter.Opponent.transform.DOMoveY(5f, 1f).OnComplete(() => { _fighter.Opponent.gameObject.SetActive(false); });
        yield return new WaitForSeconds(1f);
        //_fighter._spriteRenderer.enabled = false; 
        fighterOponentAnimator.transform.localScale = _fighter.Inverted ? new Vector3(1f, 1f, 1f) : new Vector3(-1f, 1f, 1f);
        fighterOponentAnimator.transform.localPosition += _fighter.Inverted ? new Vector3(100f, 0, 0) : new Vector3(50, 0, 0);
        _whiteBG.transform.parent.gameObject.SetActive(true);
        _whiteBG.DOColor(Color.white, 1f);
        yield return new WaitForSeconds(1);
        Color col = Color.white;
        col.a = 0;
        _whiteBG.DOColor(col, 1f);
        fatalityObject.gameObject.SetActive(true);
        fighterOponentAnimator.Play(f.FighterID);
        yield return new WaitForSeconds(3);
        fighterOponentAnimator.SetTrigger("death");
        pickImage.sprite = pickWithBlood;
        Vector3 bloodPos = Camera.main.ScreenToWorldPoint(fighterOponentAnimator.transform.position);
        GameObject bloodObj = Instantiate(bloodFXPrefab, fighterOponentAnimator.transform);
        bloodObj.transform.localPosition = Vector3.zero + new Vector3(0, 0, 0);
        OnFatalityFinished.Invoke();
    }
    #endregion /Fatality
}