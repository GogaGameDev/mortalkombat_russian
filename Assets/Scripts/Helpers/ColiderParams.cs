using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Colider Params", menuName = "Game/Colider Params")]
public class ColiderParams : ScriptableObject
{
    public Vector2 offset = Vector2.zero;
    public Vector2 size = Vector2.zero;
}