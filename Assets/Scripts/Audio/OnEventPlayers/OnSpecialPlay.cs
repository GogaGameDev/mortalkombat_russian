﻿using UnityEngine;

namespace Audio
{
    public class OnSpecialPlay : OnEventPlay
    {
        [SerializeField] private SpecialBase _specialBase;
        [SerializeField] private int _specialType;

        protected override void SubscribeToEvent()
        {
            switch (_specialType)
            {
                case 1:
                    _specialBase.OnSpecial_1_Started.AddListener(PlaySound);
                    break;
                case 2:
                    _specialBase.OnSpecial_2_Started.AddListener(PlaySound);
                    break;
                default:
                    _specialBase.OnSpecial_1_Started.AddListener(PlaySound);
                    _specialBase.OnSpecial_2_Started.AddListener(PlaySound);
                    break;
            }
        }
    }
}