using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " New FighterList", menuName = "Game/FighterList")]
public class FighterList : ScriptableObject
{
    public List<FighterInLine> _fighters;
}

[System.Serializable]
public struct FighterInLine
{
    public string _fighterID;
    public bool hideName;
    public string _fighterDisplayName;
    public Sprite _fighterIcon;
    public FighterUISelection _fighterPrefab;
    public Fighter _ingameFighterPrefab;
}