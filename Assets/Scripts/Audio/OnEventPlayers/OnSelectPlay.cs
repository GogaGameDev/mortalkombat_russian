﻿using UnityEngine;

namespace Audio
{
    public class OnSelectPlay : OnEventPlay
    {
        [SerializeField] private PlayerInputUI _playerInputUI;

        protected override void SubscribeToEvent()
        {
            _playerInputUI.OnSelect.AddListener(PlaySound);
        }

        private void PlaySound(int arg0)
        {
            PlaySound();
        }
    }
}