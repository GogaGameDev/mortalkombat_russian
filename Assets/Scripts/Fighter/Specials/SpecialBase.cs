using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpecialBase : MonoBehaviour
{
    protected Fighter _fighter = null;
    protected FighterCombat _combat = null;

    [SerializeField] protected float _special_Cooldown = 4;
    [SerializeField] protected bool _canDoSpecial = true;


    public bool CanDoSpecial => _canDoSpecial;

    [HideInInspector] public UnityEvent OnFatalityStarted = new UnityEvent();
    [HideInInspector] public UnityEvent OnFatalityFinished = new UnityEvent();
    [HideInInspector] public UnityEvent OnSpecial_1_Started = new UnityEvent();
    [HideInInspector] public UnityEvent OnSpecial_2_Started = new UnityEvent();

    public virtual void Init(Fighter fighter, FighterCombat combat)
    {
        _fighter = fighter;
        _combat = combat;
    }
    public virtual void Special_1(Vector3 spawnPos)
    {
        _canDoSpecial = false;
        OnSpecial_1_Started.Invoke();
        StartCoroutine(CoolDownSPecial(1));
    }

    public virtual void Special_2(Vector3 spawnPos)
    {
        _canDoSpecial = false;
        OnSpecial_2_Started.Invoke();
        StartCoroutine(CoolDownSPecial(2));
    }

    public virtual void Fatality()
    {
        _fighter.Inputs.ToggleControls(false);
    }

    protected IEnumerator CoolDownSPecial(int specialID)
    {
        yield return new WaitForSeconds(_special_Cooldown);
        _canDoSpecial = true;
    }
    public virtual void ResetSpecials()
    {

    }

}