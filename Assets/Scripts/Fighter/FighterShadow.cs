using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FighterShadow : MonoBehaviour
{
    [SerializeField] Fighter _target = null;
    [SerializeField] Vector3 _offset = Vector3.zero;
    [SerializeField] float floorPosY = -1.25f;

    // Update is called once per frame
    void LateUpdate()
    {
        if (_target != null)
        {
            Vector3 newPos = _target.transform.position + (_target.Inverted ? transform.right : -transform.right) * 0.075f;
            newPos.y = floorPosY;
            transform.position = newPos;
        }
        if (!_target.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
    }
    public void Init(Fighter target)
    {
        gameObject.name = target.gameObject.name + "_Shadow";
        _target = target;

        target.OnGetup.AddListener(() =>
        {
            transform.DOScaleX(1f, 0.5f);
        });

        target.OnKnockedDown.AddListener(() =>
        {
            transform.DOScaleX(1.5f, 0.5f);
        });
     
    }


}