using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class FighterUISelection : MonoBehaviour
{
    SpriteRenderer _spriteRend = null;
    Animator _animator = null;
    int _playerID = -1;
    [SerializeField] string _fighterID = "";
    public string FighterID => _fighterID;
    public bool HideName;
    void Start()
    {
        _spriteRend = this.GetComponent<SpriteRenderer>();
        _animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateColider(ColiderParams coliderParams)
    {


    }
    public void DoingSpecial(string isDoingSpecial)
    {


    }
    public void ShowMachine(string isDoingSpecial)
    {


    }
    public void SetState(string stateName)
    {
        if (_animator == null)
        {
            _animator = this.GetComponent<Animator>();
        }
        _animator.Play(stateName);
    }
    public void Init(int playerID, string fighterID)
    {
        _playerID = playerID;
        _fighterID = fighterID;
        _spriteRend = this.GetComponent<SpriteRenderer>();
        _spriteRend.flipX = playerID == 1 ? false : true;

    }

    public void SetAnimation(string trigger)
    {
        if (_animator == null)
        {
            _animator = this.GetComponent<Animator>();
        }
        _animator.SetTrigger(trigger);
    }
    public void SetAnimation(string boolStringValue, bool value)
    {
        if (_animator == null)
        {
            _animator = this.GetComponent<Animator>();
        }
        _animator.SetBool(boolStringValue, value);
    }

    public void Select() 
    {
        SetAnimation("win_trigger");
        ResetColor();
    }
    public void ResetColor() 
    {    
        if (_fighterID != "GARAEV")
        {
            return;
        }
        _spriteRend.DOColor(Color.white, 0.25f);
    }
}