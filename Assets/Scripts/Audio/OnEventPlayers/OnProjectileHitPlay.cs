﻿using UnityEngine;

namespace Audio
{
    public class OnProjectileHitPlay : OnEventPlay
    {
        [SerializeField] private Projectile _projectile;
        protected override void SubscribeToEvent()
        {
            _projectile._OnHit.AddListener(PlaySound);
            transform.SetParent(transform.parent.parent);
        }

        private void PlaySound(GameObject arg0)
        {
            PlaySound();
        }
    }
}