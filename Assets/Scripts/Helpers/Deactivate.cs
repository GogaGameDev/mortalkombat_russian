using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Deactivate : MonoBehaviour
{
    [SerializeField] float _time;
    Animator anim = null;
    public UnityEvent OnDeactivate;

    private void Start()
    {
        anim = this.GetComponent<Animator>();
    }
    private void OnEnable()
    {
        if (_time != -1)
        {
            StartCoroutine(ShowHide());
        }

    }

    IEnumerator ShowHide()
    {
        yield return new WaitForSeconds(_time);
        anim.SetTrigger("deactivate_trigger");
    }

    public void DeactivateObject()
    {
        this.gameObject.SetActive(false);
        OnDeactivate.Invoke();
    }
}