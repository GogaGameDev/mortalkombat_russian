﻿using UnityEngine;

namespace Audio
{
    public class OnHitPunchPlay : OnEventPlay
    {
        [SerializeField] private Fighter _fighter;
        protected override void SubscribeToEvent()
        {
            _fighter.OnHit.AddListener(PlaySound);
        }

        private void PlaySound(HitType arg0)
        {
            PlaySound();
        }
    }
}