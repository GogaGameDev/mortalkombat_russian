using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Arena : MonoBehaviour
{
    [SerializeField] SpriteRenderer[] _sprites;
    [SerializeField] Color fatalityStartColor;

    public void FadeBackround()
    {
        foreach (var item in _sprites)
        {
            item.DOColor(fatalityStartColor, 1f);
        }
    }
}