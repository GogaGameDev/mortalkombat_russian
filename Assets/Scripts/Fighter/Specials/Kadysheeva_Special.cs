using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class Kadysheeva_Special : SpecialBase
{
    [SerializeField] float _special_1_Damage = 10f;
    [SerializeField] float _special_2_Damage = 10f;
    Coroutine _special_1_routine = null;
    Coroutine _special_2_routine = null;
    [SerializeField] LineRenderer laser_renderer = null;
    Coroutine _laserRoutine = null;
    [SerializeField] float _laserDistance=1.15f;
    [SerializeField] Color laserColor = Color.green;
    Fighter _opponent = null;
    public override void Init(Fighter f, FighterCombat c)
    {
        base.Init(f, c);

#if UNITY_EDITOR
        if (_fighter._playerID == 1)
        {
            // Fatality();
        }
#endif

    }

    #region Specials
    public override void ResetSpecials()
    {
        base.ResetSpecials();

        if (_special_1_routine != null)
        {
            StopCoroutine(_special_1_routine);
            _special_1_routine = null;
            _fighter.IsDoingSpecial = false;
            _fighter.RB.constraints = RigidbodyConstraints2D.FreezeRotation;
            if (_opponent != null) 
            {
                _opponent.UpdateColiderEnable(true);
            }
        }
        if (_special_2_routine != null)
        {
            StopCoroutine(_special_2_routine);
            _special_2_routine = null;
            _fighter.IsDoingSpecial = false;
            if (_laserRoutine != null) {
                StopCoroutine(_laserRoutine);
            }
                _laserRoutine = StartCoroutine(MoveLaser(0.5f, laser_renderer.GetPosition(1), 0, 35f));
            
        }

    }
    public override void Special_1(Vector3 spawnPos)
    {

#if UNITY_EDITOR
        //Debug.LogWarning(this.gameObject.name + "Special_1");
#endif
        if (!CanDoSpecial)
        {
            return;
        }
        base.Special_1(spawnPos);
      _special_1_routine =  StartCoroutine(Special_1Routine(spawnPos));

        //_combat.
    }
    IEnumerator Special_1Routine(Vector3 spawnPos)
    {
        _fighter.Animation.SetState("kadysheeva_special_1_start");
        _fighter.transform.DOMoveY(5f, 0.8f);
        _fighter.RB.constraints = RigidbodyConstraints2D.FreezeAll;
        yield return new WaitForSeconds(1f);
        _fighter.transform.position = new Vector3(_fighter.Opponent.transform.position.x, _fighter.transform.position.y, _fighter.transform.position.z);
        _fighter.transform.DOMoveY(_fighter.LandingPosY,0.5f);
        yield return new WaitForSeconds(0.2f);
        _fighter.UpdateColiderEnable(false);
        _fighter.Animation.SetState("kadysheeva_special_1_end");
         _opponent = null;
        Debug.Log("distance between fighters :" + Vector3.Distance(_fighter.transform.position, _fighter.Opponent.transform.position));

        if (Vector3.Distance(_fighter.transform.position, _fighter.Opponent.transform.position) < 2f)
        {
            _opponent = _fighter.Opponent.gameObject.GetComponent<Fighter>();
        }


        if (_opponent != null)
        {

            _opponent.ResetAllActiveStatus();
            _opponent.TakeDamage(_special_1_Damage, AttackType.Special);
            _opponent.Inputs.ToggleControls(false);
            yield return new WaitForSeconds(0.1f);
            _opponent.Animation.SetState("lay_down");
            _opponent.IsUnderSpecial = true;
            yield return new WaitForSeconds(0.5f);
            _fighter.Animation.SetState("kadysheeva_jump_back");
            _fighter.PushBack(1.25f, 1f, 0.5f);
            yield return new WaitForSeconds(0.5f);
            _fighter.Animation.SetTrigger("IDL");
            _fighter.RB.constraints = RigidbodyConstraints2D.FreezeRotation;
            _fighter.UpdateColiderEnable(true);
            yield return new WaitForSeconds(0.5f);
            _opponent.IsUnderSpecial = false;
            _opponent.Animation.SetState("get_up_anim");
            _fighter.IsDoingSpecial = false;
            _fighter.Inputs.ToggleControls(true);
            yield return new WaitForSeconds(0.5f);
            _opponent.Inputs.ToggleControls(true);
            _fighter.ResetAllActiveStatus();

        }
        else 
        {
            _fighter.IsDoingSpecial = false;
            _fighter.ResetAllActiveStatus();
        }

    }
    public override void Special_2(Vector3 spawnPos)
    {
#if UNITY_EDITOR
        //   Debug.LogWarning(this.gameObject.name + "Special_2");
#endif

        if (!CanDoSpecial)
        {
            return;
        }

        base.Special_2(spawnPos);
        _special_2_routine= StartCoroutine(Special_2Routine(spawnPos));
    }

    RaycastHit2D hit;
    IEnumerator Special_2Routine(Vector3 spawnPos)
    {

        yield return new WaitForSeconds(0.25f);
        Vector3 origin = spawnPos;
        Vector3 direction = _fighter.Inverted ? -transform.right : transform.right;
        Vector3 offset = new Vector3(0,0 ,-5f);
        laser_renderer.positionCount = 2;
        Vector3[] positions = new Vector3[2] { origin+ offset, origin+ offset };
        laser_renderer.SetPositions(positions);
      
        if (_laserRoutine != null)
        {
            StopCoroutine(_laserRoutine);
            _laserRoutine = null;
        }

        _laserRoutine = StartCoroutine(MoveLaser(0, origin + direction.normalized * _laserDistance + offset, 1,25f));
        _fighter.Animation.SetState("kadysheeva_special_2_loop");
        LayerMask mask = LayerMask.GetMask(_fighter._playerID == 1 ? "p2" : "p1");
        Debug.DrawRay(origin, direction.normalized * _laserDistance, Color.red, 3f);
        hit = Physics2D.Raycast(origin, direction, _laserDistance, mask);
        if (hit.collider != null)
        {
            StopCoroutine(_laserRoutine);
            Vector3 newLaserEndPos = laser_renderer.GetPosition(0);
            newLaserEndPos.x = hit.collider.transform.position.x + (_fighter.Inverted?-0.25f:0.25f);
            _laserRoutine = StartCoroutine(MoveLaser(0, newLaserEndPos + offset, 1, 40f));

            Fighter f = hit.collider.gameObject.GetComponent<Fighter>();
            bool onGround = f.IsOnGround;
            if (f.IsBlocking|| f.IsKnockedDown)
            {
                yield return new WaitForSeconds(0.2f);
                if (_laserRoutine != null)
                {
                    StopCoroutine(_laserRoutine);
                    _laserRoutine = null;
                }
                _laserRoutine = StartCoroutine(MoveLaser(0f, laser_renderer.GetPosition(1), 0, 50));
                _fighter.ResetAllActiveStatus();
                yield break;
            }
            if (onGround)
            {
                f.TakeDamage(_special_2_Damage, AttackType.Special);
            }
           
            f.GetComponent<SpriteRenderer>().color = laserColor;
            yield return new WaitForSeconds(0.1f);
            f.GetComponent<Animator>().enabled = false;
            _fighter.Inputs.ToggleControls(false);
            f.Inputs.ToggleControls(false);
            yield return new WaitForSeconds(0.1f);
            f.ResetAllActiveStatus();
            f.transform.DOMoveX(f.transform.position.x + 0.07f, 0.075f).SetLoops(-1, LoopType.Yoyo);
            _fighter.Animation.SetState("kadysheeva_special_2_loop");
            yield return new WaitForSeconds(1f);
            f.GetComponent<Animator>().enabled = true;
            f.GetComponent<SpriteRenderer>().color = Color.white;
            f.transform.DOKill();
            _fighter.Inputs.ToggleControls(true);
            f.Inputs.ToggleControls(true);
            if (!onGround)
            {
                yield return new WaitForSeconds(0.1f);
                f.TakeDamage(_special_2_Damage, AttackType.Hand_Low_UpperCut);
            }

            if (_laserRoutine != null)
            {
                StopCoroutine(_laserRoutine);
                _laserRoutine = null;
            }
            _laserRoutine = StartCoroutine(MoveLaser(0f, laser_renderer.GetPosition(1), 0, 50));
            _fighter.ResetAllActiveStatus();
            yield break;
        }
        else 
        {
            yield return new WaitForSeconds(0.75f);
        }

        if (_laserRoutine != null) 
        {
            StopCoroutine(_laserRoutine);
            _laserRoutine = null;
        }
     _laserRoutine = StartCoroutine(MoveLaser(0.5f,laser_renderer.GetPosition(1),0,35f));
        _fighter.ResetAllActiveStatus();
    }

    IEnumerator MoveLaser(float delay, Vector3 endPosition, int laserEndID, float speed = 20f) 
    {
        yield return new WaitForSeconds(delay);
       
        if (laser_renderer.positionCount < 0) 
        {
            yield break;
        }
        
        while (true)
        {  
            Vector3 OldPos = laser_renderer.GetPosition(laserEndID);
            Vector3 newPos = Vector3.Lerp(OldPos, endPosition, speed * Time.fixedDeltaTime);
            float distance = Vector3.Distance(laser_renderer.GetPosition(laserEndID), endPosition);
           
            if (distance < 0.05f)
            {
                break;
            }
            laser_renderer.SetPosition(laserEndID, newPos);
           
           
            yield return new WaitForSeconds(0.1f);

        }

        laser_renderer.SetPosition(laserEndID, endPosition);
    }

    #endregion /Specials
    #region Fatality
    [Space(10)]
    [Header("Fatality")]
    [SerializeField] Chorovod chorovod;
    [SerializeField] GameObject _bodyPartsPrefab;
    public override void Fatality()
    {
        base.Fatality();
        // fighterImage.sprite=
#if UNITY_EDITOR
        Debug.LogWarning(this.gameObject.name + "Fatality");
#endif
        StartCoroutine(FatalityRoutine());
    }

    IEnumerator FatalityRoutine()
    {
        OnFatalityStarted.Invoke();
        // move to position

        Vector3 pos = _fighter.Opponent.position - (_fighter.Opponent.position - _fighter.transform.position).normalized*2.2f;
        float moveDelay = Mathf.Abs(transform.position.x - pos.x) / 1.1f;
        _fighter.MoveToPosition(pos, moveDelay);
        yield return new WaitForSeconds(moveDelay);
        // code here
        _fighter.Animation.SetState("fatality_pose");
        yield return new WaitForSeconds(0.75f);
        Camera.main.GetComponent<CameraMover>().MoveToPoseX(_fighter.Opponent.position.x,1f);
        chorovod.transform.localScale = new Vector3(_fighter.Inverted ? -chorovod.transform.localScale.x : chorovod.transform.localScale.x, chorovod.transform.localScale.y, chorovod.transform.localScale.z);
        chorovod.transform.localPosition = new Vector3(_fighter.Inverted?-chorovod.transform.localPosition.x+0.35f:chorovod.transform.localPosition.x,chorovod.transform.localPosition.y,chorovod.transform.localPosition.z);
        chorovod.gameObject.SetActive(true);
        yield return new WaitForEndOfFrame();
        chorovod.OnHideFighter.AddListener(() => 
        { 
            _fighter.Opponent.gameObject.SetActive(false);
            Instantiate(_bodyPartsPrefab, _fighter.Opponent.transform.position + new Vector3(0, -0.25f, 0), Quaternion.identity);
        });
        
        yield return new WaitForSeconds(3f);
        chorovod.gameObject.SetActive(false);
        _fighter.MoveToPosition(_fighter.Opponent.position, 1f);
        yield return new WaitForSeconds(0.5f);
        OnFatalityFinished.Invoke();

    }

    #endregion /Fatality
}