
public enum Direction
{
    Left,
    Right,
    Up,
    Down

}
public enum AttackType
{
    Hand_Mid,
    Hand_Low,
    Hand_Low_UpperCut,
    Hand_Air,
    Leg_Mid,
    Leg_Low,
    Leg_Air,
    Special
}

public enum FighterState
{
    IDL,
    Walking,
    Attack,
    Hit,
    Crouch,
    Jump,
    Block
}
public enum HitType
{
    Low,
    Mid,
    UpperCut
}
public enum SpecialDirection
{
    Forward,
    Backward
}
public enum AttackKeys
{
    X,
    B,
    Y,
    A
}
public enum Gender
{
    Male,
    Female
}