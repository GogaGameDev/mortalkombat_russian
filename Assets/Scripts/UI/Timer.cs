using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class Timer : MonoBehaviour
{
    [SerializeField] int _timerTime = 60;
    [SerializeField] Sprite[] _numberSpriteList = null;
    [SerializeField] Image first_Image;
    [SerializeField] Image second_Image;
    Coroutine _timerRoutine = null;
    public UnityEvent TimesUp;

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.T)) 
        {
            if (_timerRoutine != null)
            {
                StopTimer();

            }
            else 
            {
                StartTimer();
            }
        }
#endif
    }
    private void Start()
    {
        //  SetTime();
    }
    public void StartTimer()
    {

        if (_timerRoutine != null)
        {
            StopCoroutine(_timerRoutine);
        }
        _timerRoutine = StartCoroutine(TimerRoutine());
    }

    public void SetTime(int timer)
    {
        _timerTime = timer;
#if UNITY_EDITOR
       // _timerTime = 5;
#endif
        if (timer <= 0)
        {
            return;
        }

        int first_ID = _timerTime / 10;
        int second_ID = _timerTime - first_ID * 10;
        first_Image.sprite = _numberSpriteList[first_ID];
        second_Image.sprite = _numberSpriteList[second_ID];
    }
    IEnumerator TimerRoutine()
    {
        if (_timerTime <= 0)
        {
            yield break;
        }
        while (_timerTime >= 0)
        {
            yield return new WaitForSeconds(1);
            int first_ID = _timerTime / 10;
            int second_ID = _timerTime - first_ID * 10;
            first_Image.sprite = _numberSpriteList[first_ID];
            second_Image.sprite = _numberSpriteList[second_ID];
            _timerTime--;

        }

        TimesUp.Invoke();
    }
    public void StopTimer()
    {
        if (_timerRoutine != null)
        {
            StopCoroutine(_timerRoutine);

        }
    }
}